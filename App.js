/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format

 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import Reactotron from 'reactotron-react-native';
import DropdownAlert from 'react-native-dropdownalert';

import DropdownManager from './src/common/DropdownAlert/DropdownManager';

import { colors } from './src/assets';
import MainNavigation from './src/routers/MainNavigation';
import LoadingModal from './src/common/Loading/LoadingModal';
import LoadingManager from './src/common/Loading/LoadingManager';
import NavigationService from './src/routers/NavigationService';
import RootView from './src/RootView';

import configureStore from './src/stores/configureStore';
import rootSaga from './src/sagas';
import ReactotronConfig from './src/helpers/ReactotronConfig';

const reactotron = ReactotronConfig.configure();

const sagaMonitor = Reactotron.createSagaMonitor();
const sagaMiddleware = createSagaMiddleware({ sagaMonitor });

const store = configureStore(reactotron, sagaMiddleware);
Reactotron.clear();

sagaMiddleware.run(rootSaga);

export default class App extends Component {
  componentDidMount() {
    LoadingManager.register(this.loadingRef);
    DropdownManager.register(this.dropDownAlertRef);
  }

  componentWillUnmount() {
    LoadingManager.unregister(this.loadingRef);
    DropdownManager.unregister(this.dropDownAlertRef);
  }

  render() {
    return (
      <Provider store={store}>
        <RootView>
          <MainNavigation
            ref={navigatorRef => NavigationService.setTopLevelNavigator(navigatorRef)}
          />

          <LoadingModal
            ref={ref => {
              this.loadingRef = ref;
            }}
          />

          <DropdownAlert
            inactiveStatusBarBackgroundColor={colors.primaryColor}
            // inactiveStatusBarBackgroundColor={colors.header}
            successImageSrc={null}
            infoImageSrc={null}
            warnImageSrc={null}
            errorImageSrc={null}
            ref={ref => {
              this.dropDownAlertRef = ref;
            }}
          />
        </RootView>
      </Provider>
    );
  }
}
