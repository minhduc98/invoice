import React from 'react';
import { View, Text, Picker, TextInput, StyleSheet, TouchableOpacity, Image } from 'react-native';
import R from './src/assets/R';

export default class thuyxinhgai extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      a: '',
      b: '',
      c: '',
      d: ''

    }
  }

    state = {
      reference: 'Hoan thanh',
      sign: 'Da ki',
      a: '',
      checked: 0
    }

  onPress= () => {
    this.setState({ checked: this.state.checked = 1
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Trang tai lieu</Text>

        <View style={styles.ochu}>

          <Picker
            selectedValue={this.state.reference}
            style={{ height: 40, width: 430, color: 'grey' }}
            onValueChange={(itemValue, itemIndex) => this.setState({ reference: itemValue })
  }
          >
            <Picker.Item label="Hoan thanh" value="hoanthanh" />
            <Picker.Item label="Chua hoan thanh" value="chuahoanthanh" />
          </Picker>
        </View>
        <Text style={styles.text}>Trang thai duyet</Text>
        <TextInput
          style={styles.ochu}
          onChangeText={text => { this.setState({ a: text }) }}
          placeholder="  Da duyet"
          value={this.state.a}

        />
        <Text style={styles.text}>Nguoi tao</Text>
        <TextInput
          style={styles.ochu}
          onChangeText={text => { this.setState({ b: text }) }}
          placeholder="  Ta Thi Thu Thuy"
          value={this.state.b}

        />
        <Text style={styles.text}>Nguoi cap nhat</Text>
        <TextInput
          style={styles.ochu}
          onChangeText={text => { this.setState({ c: text }) }}
          placeholder="  Khanh xinh dep"
          value={this.state.c}

        />
        <Text style={styles.text}>Trang thai ky</Text>
        <View style={styles.ochu}>

          <Picker
            selectedValue={this.state.reference}
            style={styles.picker}
            onValueChange={(itemValue, itemIndex) => this.setState({ reference: itemValue })
  }
          >
            <Picker.Item label="Da ky" value="daky" />
            <Picker.Item label="Chua ky" value="chua ki" />
          </Picker>
        </View>
        <View style={{ height: 50, flexDirection: 'row' }}>
          <TouchableOpacity
            onPress={() => { this.setState({ checked: 1 }) }}
            style={styles.touch}
          >
            {this.state.checked === 1

              && <Image
                style={styles.touch1}
                source={{ uri: 'https://dictionary.cambridge.org/vi/images/thumb/check_noun_002_06440.jpg?version=5.0.69' }}
              />
            }


          </TouchableOpacity>

          <Text style={{ fontSize: 15, marginLeft: 10, marginTop: 20 }}> Ban ghi trinh ki</Text>
        </View>
        <Text style={styles.text}>Ma hieu van ban</Text>
        <TextInput
          style={styles.ochu}
          onChangeText={text => { this.setState({ d: text }) }}
          placeholder="  HA123"
          value={this.state.d}

        />
        <View style={styles.tao}>
          <Text style={styles.text1}>Tao to trinh</Text>
        </View>
        <Text style={styles.text2}>Kiem tra lai</Text>


      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 20
  },
  text: {
    color: R.colors.black0,
    marginTop: 20,
    fontSize: 15,
  },
  ochu: {
    height: 45,
    width: 440,
    borderRadius: 10,
    borderColor: 'grey',
    borderWidth: 1,
    marginTop: 10,
    color: 'grey',
  },
  picker: {
    height: 45, width: 430, color: 'grey', flexDirection: 'row'
  },
  touch: {
    height: 25,
    width: 25,
    borderRadius: 5,
    borderWidth: 1,
    marginTop: 20

  },
  touch1: {
    height: 25,
    width: 25,
    borderRadius: 5,

  },
  tao: {
    backgroundColor: R.colors.blue401,
    height: 50,
    width: 250,

    marginTop: 20,
    borderRadius: 10,
    marginLeft: 80,
    justifyContent: 'center'
  },
  text1: {
    color: R.colors.white, fontSize: 18, alignSelf: 'center'
  },
  text2: {
    color: R.colors.blue401, fontSize: 15, marginTop: 15, marginLeft: 165
  }
})
