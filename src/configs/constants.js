export const SOCIAL_LOGIN_TYPE = {
  FACEBOOK: 1,
  GOOGLE: 2,
};

export const GENDER = {
  MALE: 1,
  FEMALE: 2,
  OTHER: 3,
}

export const GENDER_LABEL = {
  [GENDER.MALE]: 'Nam',
  [GENDER.FEMALE]: 'Nữ',
  [GENDER.OTHER]: 'Khác',
}

export const NOTIFY_TYPE = {
};


export const ONESIGNAL_TAG = 'userId'

export const LIMIT_RECORD = {
  HOME: 10,
  DEFAULT: 20,
}

export const USER_ROLE = {
  TEACHER: 2,
  MONITOR: 3,
  PARENT: 4,
}

export const NAME_BY_TYPE = {
  GO_TO_SCHOOL: 'Đến trường',
  GO_HOME: 'Về nhà'
}

export const MUSTER_STATUS = {
  ABSENT: 'ABSENT',
  PRESENT: 'ENTER',
  UNKNOWN: 'UNKNOWN'
}

// export const MUSTER_TYPE = {
//   IN_CLASS: 1,
//   PRESENT: 2
// }

export const MUSTER_TYPE = {
  TIME_ON_BUS_TO_SCHOOL: 'TIME_ON_BUS_TO_SCHOOL', // lên xe đi
  TIME_OUT_BUS_TO_SCHOOL: 'TIME_OUT_BUS_TO_SCHOOL', // xuống xe đi
  TIME_AT_SCHOOL: 'TIME_AT_SCHOOL', // vào cổng
  TIME_OUT_SCHOOL: 'TIME_OUT_SCHOOL', // ra cổng
  TIME_IN_CLASS: 'TIME_IN_CLASS', // vào lớp
  TIME_OUT_CLASS: 'TIME_OUT_CLASS', // ra lớp
  GO_TO_SCHOOL: 'GO_TO_SCHOOL', // đến trường
  OUT_OF_SCHOOL: 'OUT_OF_SCHOOL', //  rời trường
  AT_SCHOOL: 'AT_SCHOOL', // ở trường
  IN_CLASS: 'IN_CLASS', // ở lớp
  GO_HOME: 'GO_HOME', // về nhà
  TIME_ON_BUS_GO_HOME: 'TIME_ON_BUS_GO_HOME', // lên xe về
  TIME_OUT_BUS_GO_HOME: 'TIME_OUT_BUS_GO_HOME', // xuống xe về
  UNKNOWN: 'UNKNOWN'
}

export const MUSTER_LABEL = {
  [MUSTER_TYPE.TIME_ON_BUS_TO_SCHOOL]: 'Lên xe đến trường', // lên xe đi
  [MUSTER_TYPE.TIME_OUT_BUS_TO_SCHOOL]: 'Xuống xe đến trường', // xuống xe đi
  [MUSTER_TYPE.TIME_AT_SCHOOL]: 'Vào trường', // vào cổng
  [MUSTER_TYPE.TIME_OUT_SCHOOL]: 'Ra khỏi cổng trường', // ra cổng
  [MUSTER_TYPE.TIME_IN_CLASS]: 'Vào lớp', // vào lớp
  [MUSTER_TYPE.TIME_OUT_CLASS]: 'Ra khỏi lớp', // ra lớp
  [MUSTER_TYPE.TIME_ON_BUS_GO_HOME]: 'Lên xe về', // lên xe về
  [MUSTER_TYPE.TIME_OUT_BUS_GO_HOME]: 'Xuống xe về', // xuống xe về
  [MUSTER_TYPE.UNKNOWN]: 'unknown'
}

export const MUSTER_DATA = [
  {
    id: 1,
    name: 'Lên xe đi',
    active: false,
    type: MUSTER_TYPE.TIME_ON_BUS_TO_SCHOOL
  },
  {
    id: 2,
    name: 'Xuống xe đi',
    active: false,
    type: MUSTER_TYPE.TIME_OUT_BUS_TO_SCHOOL
  },
  {
    id: 3,
    name: 'Vào cổng',
    active: false,
    type: MUSTER_TYPE.TIME_AT_SCHOOL
  },
  {
    id: 4,
    name: 'Vào lớp',
    active: false,
    type: MUSTER_TYPE.TIME_IN_CLASS
  },
  {
    id: 5,
    name: 'Ra cổng',
    active: false,
    type: MUSTER_TYPE.TIME_OUT_SCHOOL
  },
  {
    id: 6,
    name: 'Lên xe về',
    active: false,
    type: MUSTER_TYPE.TIME_ON_BUS_GO_HOME
  },
  {
    id: 7,
    name: 'Xuống xe về',
    active: false,
    type: MUSTER_TYPE.TIME_OUT_BUS_GO_HOME
  },
]
