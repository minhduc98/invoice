import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');
// export const WIDTH = (w) => width * (w / 360);
// export const HEIGHT = (h) => height * (h / 640);
export const WIDTH = (w) => width * (w / 375);
export const HEIGHT = (h) => height * (h / 812);
export const getFont = (f) => f - 1;
export const getLineHeight = (f) => f;
