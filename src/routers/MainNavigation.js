import { createStackNavigator, createAppContainer } from 'react-navigation';
import * as screenNames from './screenNames';
import DangNhaps from '../features/DangNhap/DangNhap'
import PheDuyetDon from '../features/PheDuyetDon/PheDuyetDon';
import LocTimKiem from '../features/PheDuyetDon/LocTimKiem';
import TaoToTrinh from '../features/TaoToTrinh/TaoToTrinh';
import TaoToTrinhPage2 from '../features/TaoToTrinh/TaoToTrinhPage2'
import TaoToTrinhPage3 from '../features/TaoToTrinh/TaoToTrinhPage3'
import Picker from '../features/TaoToTrinh/Picker'
import PheDuyetToTrinh from '../features/PheDuyetToTrinh/PheDuyetToTrinh';
import Intro from '../features/DangNhap/Intro';
import Home2 from '../features/Home/Home2';
import Home1 from '../features/Home/Home1';
import Home3 from '../features/Home/Home3';
import Profile from '../features/Profile/Profile';

console.disableYellowBox = true

const AppNavigator = createStackNavigator(
  {
    Intro: { screen: Intro },
    DangNhaps: { screen: DangNhaps },
    PheDuyetDon: { screen: PheDuyetDon },
    LocTimKiem: { screen: LocTimKiem },
    TaoToTrinh: { screen: TaoToTrinh },
    Picker: { screen: Picker },
    TaoToTrinhPage2: { screen: TaoToTrinhPage2 },
    TaoToTrinhPage3: { screen: TaoToTrinhPage3 },
    PheDuyetToTrinh: { screen: PheDuyetToTrinh },

    //
    Home1: { screen: Home1 },
    Home2: { screen: Home2 },
    Home3: { screen: Home3 },
    Profile: { screen: Profile }
  },
  {
    initialRouteName: 'Intro',
    headerMode: 'none'
  }
);
export default createAppContainer(AppNavigator);
