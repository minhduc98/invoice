const fonts = {
  title: 'Arial',
  text: 'SanFrancisco',
  code: 'Fira',
  Roboto: 'Roboto'
};
export default fonts;
