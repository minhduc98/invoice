import * as vi from './languages/vi.js' // string tiếng việt
import * as en from './languages/en.js' // string tiếng anh

export const languages = { en, vi }
export { default as images } from './images'
export { default as colors } from './colors'
