/* eslint-disable no-param-reassign */
/* eslint-disable no-shadow */
/* eslint-disable no-undef */
import i18n from 'i18n-js';
import AsyncStorage from '@react-native-community/async-storage';
import { I18nManager } from 'react-native';
import vi from './languages/vi';
import en from './languages/en';

i18n.translations = {
  vi,
  en
};
i18n.fallbacks = true;
const defaultLanguage = { languageTag: 'vi', isRTL: false };
const { languageTag, isRTL } = defaultLanguage;
I18nManager.forceRTL(isRTL);
i18n.locale = languageTag;

export default i18n

export function setLocation(i18n, location) {
  _storeData(location)
  const defaultLanguage = { languageTag: location, isRTL: false };
  const { languageTag, isRTL } = defaultLanguage;
  i18n.locale = languageTag;
  return i18n
}

_storeData = async (location) => {
  try {
    await AsyncStorage.setItem('LANGUAGE', location);
  } catch (error) {
  }
};
