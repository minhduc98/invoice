/* eslint-disable global-require */

const images = {
  bg_cannot_connect: require('./images/bg_cannot_connect.png'),
  app_icon: require('./images/app_icon.png'),
  ic_airbnb: require('./images/ic_airbnb.png'),
  ic_upgrade: require('./images/ic_upgrade.png'),
  noData: require('./images/noData.png'),

  // Duc chuyen
  Logo: require('./images/Logo.png'),
  VanTay: require('./images/VanTay.png'),
  Filter: require('./images/filter.png'),
  Add: require('./images/add.png'),
  IconRight: require('./images/iconRight.png'),
  IconBack: require('./images/iconBack.png'),

  /**
   * Login
   */
  loginLogo: require('./images/loginImage.png'),
  loginText: require('./images/text.png'),
  avatar: require('./images/avatar.png'),
  /**
   * flash
   */
  general_car1: require('./images/flash/general_car1.png'),
  /**
   * Safe
  */
  addIcon: require('./images/add_icon.png'),
  dotRed: require('./images/dotRed.png'),
  dotBlue: require('./images/dotBlue.png'),
  dotYellow: require('./images/dotYellow.png'),
  /**
   * account
   */
  back: require('./images/account/back.png'),
  key: require('./images/account/key.png'),
  facebook: require('./images/account/facebook.png'),
  google: require('./images/account/google.png'),
  user: require('./images/account/user.png'),
  userActive: require('./images/account/userActive.png'),
  phone: require('./images/account/phone.png'),
  unselect: require('./images/account/unselect.png'),
  eye: require('./images/account/eye.png'),
  eyeAT: require('./images/account/eyeAT.png'),
  checkBox: require('./images/account/checkBox.png'),
  unCheckBox: require('./images/account/unCheckBox.png'),
  arrowLeft: require('./images/account/arrowLeft.png'),

  /**
   * Home
   */
  tabHome: require('./images/home/tabHome.png'),
  tabHomeActive: require('./images/home/tabHomeActive.png'),
  studentMale: require('./images/home/studentMale.png'),
  studentFemale: require('./images/home/studentFemale.png'),
  news: require('./images/home/news.png'),
  newsAT: require('./images/home/newsAT.png'),
  history: require('./images/home/history.png'),
  historyAT: require('./images/home/historyAT.png'),
  notify: require('./images/home/notify.png'),
  notifyAT: require('./images/home/notifyAT.png'),
  userGroup: require('./images/home/userGroup.png'),
  userGroupAT: require('./images/home/userGroupAT.png'),
  circle: require('./images/home/circle.png'),
  circleAT: require('./images/home/circleAT.png'),
  general_car2: require('./images/home/general_car2.png'),
  bgNewsTeacher: require('./images/home/bgNewsTeacher.png'),
  bars: require('./images/home/bars.png'),
  circleCheck: require('./images/home/circleCheck.png'),
  present: require('./images/home/present.png'),
  warning: require('./images/home/warning.png'),
  userClass: require('./images/home/userClass.png'),
  clock: require('./images/home/clock.png'),
  spinner: require('./images/home/spinner.png'),
  next: require('./images/home/next.png'),
  prev: require('./images/home/prev.png'),
  itemMenu1: require('./images/home/ToTrinh.png'),
  itemMenu2: require('./images/home/DeNghiTT.png'),
  itemMenu3: require('./images/home/BangTHTT.png'),
  itemMenu4: require('./images/home/HoaDon.png'),
  all: require('./images/home/all.png'),
  btnHome: require('./images/home/btnHome.png'),
  btnCam: require('./images/home/btnCam.png'),
  circleFake: require('./images/home/circleFake.png'),

  /**
   * notify
   */
  icNotify: require('./images/notify/icNotify.png'),
  icNotifyAT: require('./images/notify/icNotifyAT.png'),
  bellPlus: require('./images/notify/bellPlus.png'),

  /**
   * history
   */
  imgHistory: require('./images/history/imgHistory.png'),
  AT_SCHOOL: require('./images/history/AT_SCHOOL.png'),
  GO_HOME: require('./images/history/GO_HOME.png'),
  IN_CLASS: require('./images/history/IN_CLASS.png'),
  GO_TO_SCHOOL: require('./images/history/GO_TO_SCHOOL.png'),
  /**
   * tmp
   */
  map: require('./images/tmpImage/map.png')
};

export default images;
