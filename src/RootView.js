import React, { Component } from 'react';
import { View, StyleSheet, SafeAreaView } from 'react-native';

import OneSignal from 'react-native-onesignal';
import { NOTIFY_TYPE, USER_ROLE } from './configs/constants';
import { HomeParentScreen, HomeTeacherScreen, FlashScreen } from './routers/screenNames';
import NavigationService from './routers/NavigationService';
import AsyncStorageUtils from './helpers/AsyncStorageUtils';
import NoInternetComponent from './common/NoInternet';
import VersionChecker from './common/VersionChecker';
import StoreRatingModal from './common/StoreRating/StoreRatingModal';
import env from './env'

class RootView extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    OneSignal.init(env.oneSignalKey);

    OneSignal.inFocusDisplaying(2)
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived = (notification) => {
  }

  onOpened = async ({ notification, action }) => {
    let role = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.ROLE)

    // if (role) role = parseInt(role)
    // setTimeout(() => {
    //   let { additionalData } = notification.payload
    //   if (additionalData) {
    //     let { notifyType } = additionalData
    //     switch (notifyType) {
    //       default:
    //         // NavigationService.navigate()
    //         if (role === USER_ROLE.PARENT) NavigationService.reset(HomeParentScreen)
    //         if (role === USER_ROLE.TEACHER || role === USER_ROLE.MONITOR) NavigationService.reset(HomeTeacherScreen)
    //         if (role === null) NavigationService.reset(FlashScreen)
    //         break
    //     }
    //   }
    // }, 1000)
  }

  onIds = (device) => {

  }

  render() {
    return (
      <View style={styles.container}>
        {this.props.children}

        <NoInternetComponent />

        <StoreRatingModal />
        <VersionChecker />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default RootView;
