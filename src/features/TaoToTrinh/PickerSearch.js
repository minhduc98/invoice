// @flow
import React, { Component } from 'react';
import { StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { WIDTH, HEIGHT } from '../../configs';
import ModalSearch from './ModalSearch';
import R from '../../assets/R';

type Props = {
  onFill: Function,
  checkRequire: boolean,
  title: string,
  content: Object,
  data: Array<{
    id: string,
    name: string,
    value: string
  }>
}

type State = {
  query: string,
  value: string,
  films: Object
}

class AutocompleteExample extends Component<Props, State> {
  ModalSearch: Object

  static defaultProps = {
    checkRequire: false,
    content: null,
    title: '',
    onFill: () => {}
  }

  constructor(props: Props) {
    super(props);
    this.state = {
      films: this.props.data,
      query: '',
      value: ''
    };
    this.ModalSearch
  }

  componentDidMount() {
  }

  findFilm(query: string) {
    const { films } = this.state;
    if (query === '') {
      return []
    }


    const regex = new RegExp(`${query.trim()}`, 'i');
    return films.filter(film => film.name.search(regex) >= 0);
  }

  render() {
    console.log('aaaaaaa', this.props.data)
    const { query } = this.state;
    const films = this.findFilm(query);
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => { this.ModalSearch.setModalVisible(true) }}>
          <TextInput
            style={styles.inputBox}
            value={this.state.value}
            placeholderTextColor={this.props.checkRequire && this.props.content.length === 0 ? 'red' : 'gray'}
            editable={false}
            onChangeText={value => this.setState({ value })}
            placeholder="Nhập nội dung"
          />
          <View style={{ position: 'absolute', top: HEIGHT(14), right: WIDTH(14) }}>
            <AntDesign name="search1" size={WIDTH(18)} color={R.colors.blueGrey101} />
          </View>
        </TouchableOpacity>
        <ModalSearch
          ref={ref => this.ModalSearch = ref}
          data={this.props.data}
          onFill={(value) => {
            this.setState({ value })
            this.props.onFill(value)
          }}
          title={this.props.title}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderRadius: HEIGHT(8),
    marginTop: HEIGHT(6),
    flexDirection: 'row',
    alignItems: 'center',
    width: WIDTH(339),
  },
  autocompleteContainer: {
    zIndex: 1,
    width: WIDTH(339),
  },
  itemText: {
    fontSize: 15,
    margin: 2
  },
  inputBox: {
    width: WIDTH(339),
    paddingVertical: HEIGHT(2),
    borderRadius: HEIGHT(8),
    height: HEIGHT(52),
    paddingHorizontal: WIDTH(14),
    justifyContent: 'space-between',
    borderWidth: 1,
    color: R.colors.black0,
    borderColor: R.colors.strickColor,
    flexDirection: 'row',
    alignItems: 'center'
  },
  infoText: {
    textAlign: 'center'
  },
  titleText: {
    fontSize: 18,
    fontWeight: '500',
    marginBottom: 10,
    marginTop: 10,
    textAlign: 'center'
  },
  directorText: {
    color: 'grey',
    fontSize: 12,
    marginBottom: 10,
    textAlign: 'center'
  },
  openingText: {
    textAlign: 'center'
  }
});

export default AutocompleteExample;
