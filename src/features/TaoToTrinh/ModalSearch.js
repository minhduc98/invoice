// @flow
import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import Autocomplete from 'react-native-autocomplete-input';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { getFont, HEIGHT, WIDTH } from '../../configs';
import R from '../../assets/R';

type State = {
  modalVisible: boolean,
  films: Object,
  query: string
}

type Props = {
  data: Array<{
    id: string,
    name: string,
    value: string
  }>,
  onFill: Function,
  title: string,
  checkRequire: boolean,
  content: Object
}

class ModalSearch extends Component<Props, State> {
  static defaultProps = {
    checkRequire: false,
    content: null
  }

  constructor(props: Props) {
    super(props);
    this.state = {
      modalVisible: false,
      films: this.props.data,
      query: ''
    };
  }

  setModalVisible = (visible: boolean) => {
    this.setState({
      modalVisible: visible
    })
  }


  onSubmit=() => {
    this.setState({
      modalVisible: false
    })
  }

  findFilm(query: string) {
    const { films } = this.state;
    if (query === '') {
      return films.slice(0, 5);
    }
    const regex = new RegExp(`${query.trim()}`, 'i');
    return films.filter(film => film.name.search(regex) >= 0);
  }

  render() {
    const { title } = this.props
    const { query } = this.state;
    const films = this.findFilm(query);
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
      >
        <TouchableWithoutFeedback
          onPress={() => { this.setModalVisible(false) }}
        >
          <View
            style={styles.opacity}
          >
            <TouchableWithoutFeedback>
              <View style={styles.modal}>
                <Text style={styles.title}>{title}</Text>
                <View style={styles.container}>
                  <Autocomplete
                    autoCapitalize="none"
                    autoCorrect={false}
                    inputContainerStyle={styles.inputBox}
                    placeholderTextColor={this.props.checkRequire && this.props.content.length === 0 ? 'red' : 'gray'}
                    containerStyle={styles.autocompleteContainer}
                    data={films.length === 1 && comp(query, films[0].name) ? [] : films}
        //   data={films}
                    defaultValue={query}
                    listStyle={{ width: WIDTH(339), marginLeft: 0, marginTop: 0, borderColor: R.colors.strickColor, borderBottomLeftRadius: WIDTH(8), borderBottomRightRadius: WIDTH(8) }}
                    onChangeText={text => { this.setState({ query: text, }) }}
                    placeholder="Nhập nội dung                                        "
                    renderItem={({ item, release_date }) => (
                      <TouchableOpacity
                        style={{ paddingVertical: HEIGHT(5), paddingHorizontal: WIDTH(10) }}
                        onPress={() => {
                          this.props.onFill(item.name)
                          this.setState({ query: item.name })
                          this.setModalVisible(false)
                        }}
                      >
                        <Text style={styles.itemText}>
                          {item.name}
                        </Text>
                      </TouchableOpacity>
                    )}
                  />
                  {this.state.query.length !== 0
                && <TouchableOpacity
                  onPress={() => { this.setState({ query: '' }) }}
                  style={{ position: 'absolute', top: HEIGHT(3), right: WIDTH(8), zIndex: 1, height: HEIGHT(48), width: WIDTH(30), justifyContent: 'center', alignItems: 'center' }}
                >
                  <AntDesign name="close" size={WIDTH(18)} color={R.colors.blueGrey101} />
                </TouchableOpacity>
                }
                </View>

                {/* <View style={{ alignItems: 'center' }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.onFill(query)
                      this.setModalVisible(false)
                    }}
                    style={styles.buttonLogin}
                  >
                    <Text style={styles.textBtnTao}>Xác nhận</Text>
                  </TouchableOpacity>
                </View> */}
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

export default ModalSearch;
const styles = StyleSheet.create({
  opacity: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: R.colors.black70p
  },
  modal: {
    backgroundColor: R.colors.white100,
    width: WIDTH(355),
    borderRadius: WIDTH(10),
    height: HEIGHT(500),
    paddingTop: HEIGHT(16),
    paddingBottom: HEIGHT(14),
    alignItems: 'center',
    paddingHorizontal: WIDTH(12)
  },
  body: {
    width: WIDTH(331)
  },
  buttonLogin: {
    width: WIDTH(251),
    paddingVertical: HEIGHT(14),
    backgroundColor: R.colors.colorMain,
    borderRadius: HEIGHT(8),
    marginTop: HEIGHT(25),
    alignItems: 'center',
    justifyContent: 'center',
  },
  kiemtra: {
    fontSize: getFont(16),
    color: R.colors.colorTextDetail,
    textAlign: 'center',
    marginTop: HEIGHT(14)
  },
  textBtnTao: {
    fontWeight: '500',
    color: R.colors.white,
    fontSize: getFont(16),
    textAlign: 'center'
  },
  title: {
    color: R.colors.colorMain,
    fontSize: getFont(16),
    fontWeight: 'bold',
    marginBottom: HEIGHT(8),
    marginTop: HEIGHT(12),
    alignSelf: 'center',
  },
  textBtn: {
    fontWeight: '500',
    color: R.colors.colorMain,
    fontSize: getFont(15),
    marginRight: WIDTH(11)
  },
  container: {
    borderRadius: HEIGHT(8),
    marginTop: HEIGHT(6),
    flexDirection: 'row',
    alignItems: 'center',
    width: WIDTH(339),
  },
  autocompleteContainer: {
    zIndex: 1,
    width: WIDTH(339),
  },
  itemText: {
    fontSize: 15,
    margin: 2
  },
  inputBox: {
    width: WIDTH(339),
    paddingVertical: HEIGHT(2),
    borderRadius: HEIGHT(8),
    height: HEIGHT(52),
    paddingHorizontal: WIDTH(14),
    paddingRight: WIDTH(35),
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: R.colors.strickColor,
    flexDirection: 'row',
    alignItems: 'center'
  },
  infoText: {
    textAlign: 'center'
  },
  titleText: {
    fontSize: 18,
    fontWeight: '500',
    marginBottom: 10,
    marginTop: 10,
    textAlign: 'center'
  },
  directorText: {
    color: 'grey',
    fontSize: 12,
    marginBottom: 10,
    textAlign: 'center'
  },
  openingText: {
    textAlign: 'center'
  }
});
