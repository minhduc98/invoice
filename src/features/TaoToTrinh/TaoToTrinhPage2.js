// @flow
import React from 'react';
import { View, Text, StyleSheet, ScrollView, FlatList, TouchableOpacity, TextInput } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import _ from 'lodash';
import { getFont, WIDTH, HEIGHT, popupOk } from '../../configs';
import NavigationService from '../../routers/NavigationService';
import CheckBox from './CheckBox';
import Header from '../../common/Header/Header'
import PickerSearch from './PickerSearch';
import PickerDate from './PickerDate';
import R from '../../assets/R';

const listField = [
  {
    tieuDe: 'Số văn bản VOffice',
    loai: 3,
    isRequire: false,
    data: []
  },
  {
    tieuDe: 'Ngày duyệt VOffice',
    loai: 4,
    isRequire: false,
    data: []
  },
  {
    tieuDe: 'Nội dung',
    loai: 3,
    isRequire: true,
    data: []
  },
  {
    tieuDe: 'Tổng tiền đề nghị',
    loai: 3,
    isRequire: false,
    data: []
  },
  {
    tieuDe: 'Tổng tiền được duyệt',
    loai: 3,
    isRequire: false,
    data: []
  },
  {
    tieuDe: 'Chi các hoạt động tài trợ, quỹ phúc lợi',
    loai: 5,
    isRequire: false,
    data: []
  },
  {
    tieuDe: 'Tờ trình cha',
    loai: 0,
    isRequire: false,
    data: R.strings.LOAI_TO_TRINH
  },
  {
    tieuDe: 'Đã kết thúc',
    loai: 5,
    isRequire: false,
    data: []
  },
]

type Props = {
}

type State = {
  noiDung: string,
  checked: boolean,
  donVi: string,
  taikhoan: string
}
export default class PheDuyetDon extends React.Component<Props, State> {
    state={
      noiDung: '',
      checked: false,
      donVi: '',
      taikhoan: ''
    }

    renderItem = (item: Object, index: number) => (
      <View>
        {
          _.has(item, 'loai')
          && item.loai !== 5
          && <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text style={[styles.title, item.loai < 2 && { color: R.colors.colorsBlue }]}>
              {_.has(item, 'tieuDe') ? item.tieuDe : ''}
            </Text>
            <Text style={{ paddingLeft: WIDTH(5), color: _.has(item, 'isRequire') && item.isRequire ? 'red' : R.colors.white }}>*</Text>
          </View>
        }
        {
          index === 6
          && _.has(item, 'data')
          && <PickerSearch
            title={_.has(item, 'tieuDe') ? item.tieuDe : ''}
            data={item.data}
            onFill={(text) => { this.setState({ donVi: text }) }}
          />
        }
        {index === 1 && <PickerDate />}
        {index === 5 && <CheckBox title={_.has(item, 'tieuDe') ? item.tieuDe : ''} />}
        {index === 7 && <CheckBox title={_.has(item, 'tieuDe') ? item.tieuDe : ''} />}
        {index === 2
          && <TextInput
            style={[styles.inputBox, { color: R.colors.black1a1, minHeight: HEIGHT(48) }]}
            placeholderTextColor={this.state.noiDung.length === 0 && this.state.checked ? 'red' : 'gray'}
            multiline={_.has(item, 'tieuDe') && item.tieuDe === 'Nội dung'}
            onChangeText={noiDung => this.setState({ noiDung })}
            placeholder={_.has(item, 'tieuDe') ? item.tieuDe : ''}
          />
        }
        {
          (index === 0 || index === 3 || index === 4)
          && <TextInput
            style={[styles.inputBox, { height: HEIGHT(48), }]}
            multiline={_.has(item, 'tieuDe') && item.tieuDe === 'Nội dung'}
            onChangeText={taikhoan => this.setState({ taikhoan })}
            placeholder={_.has(item, 'tieuDe') ? item.tieuDe : ''}
          />
        }
      </View>
    )

    onSubmit = () => {
      let { noiDung } = this.state
      if (noiDung.length !== 0) {
        console.log('Page 2', this.state)
        NavigationService.navigate('TaoToTrinhPage3')
      } else {
        this.setState({ checked: true })
        popupOk('Thông báo', 'Vui lòng điền đầy đủ thông tin.')
      }
    }

    render() {
      return (
        <View style={styles.container}>
          <Header title="Thêm tờ trình" number={2} />
          <ScrollView style={{ flex: 1 }}>
            <View style={{ width: WIDTH(375), paddingHorizontal: WIDTH(18), alignItems: 'center' }}>
              <View style={styles.body}>
                <FlatList
                  extraData={this.state}
                  data={listField}
                  renderItem={({ item, index }) => this.renderItem(item, index)}
                  keyExtractor={({ item, index }) => index}
                />
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: WIDTH(347), }}>
                <View />
                <TouchableOpacity
                  onPress={() => {
                    this.onSubmit()
                  }}
                  style={styles.buttonNext}
                >
                  <Text style={styles.textBtn}>Tiếp tục</Text>
                  <FontAwesome name="angle-double-right" color={R.colors.colorMain} size={HEIGHT(25)} />
                </TouchableOpacity>
              </View>

            </View>
          </ScrollView>
        </View>
      )
    }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.white,
  },
  image: {
    height: HEIGHT(139),
    width: WIDTH(200),
    marginTop: HEIGHT(76),
    marginBottom: HEIGHT(77)
  },
  body: {
    width: WIDTH(339)
  },
  inputBox: {
    width: WIDTH(339),
    paddingVertical: HEIGHT(8),
    borderRadius: HEIGHT(8),
    paddingHorizontal: WIDTH(14),
    fontSize: getFont(16),
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: R.colors.strickColor,
  },
  inputBoxMk: {
    width: WIDTH(339),
  },
  title: {
    color: R.colors.textblack,
    fontSize: getFont(16),
    fontWeight: 'bold',
    marginBottom: HEIGHT(8),
    marginTop: HEIGHT(12)
  },
  quenMk: {
    fontSize: getFont(15),
    color: R.colors.textColor,
    textAlign: 'center',
    textDecorationColor: R.colors.textColor,
    textDecorationLine: 'underline',
    marginTop: HEIGHT(14)
  },
  textTouchId: {
    fontWeight: '500',
    fontSize: getFont(15),
    color: R.colors.colorMain,
    textAlign: 'center',
    marginTop: HEIGHT(16),
    marginBottom: HEIGHT(20)
  },
  buttonNext: {
    paddingVertical: HEIGHT(6),
    paddingHorizontal: WIDTH(14),
    backgroundColor: R.colors.white,
    borderRadius: HEIGHT(100),
    borderColor: R.colors.colorMain,
    borderWidth: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: HEIGHT(20)
  },
  textBtn: {
    fontWeight: '500',
    color: R.colors.colorMain,
    fontSize: getFont(15),
    marginRight: WIDTH(11)
  },
  imgTouch: {
    height: HEIGHT(48),
    width: WIDTH(48),
    marginTop: HEIGHT(64),
  }
})
