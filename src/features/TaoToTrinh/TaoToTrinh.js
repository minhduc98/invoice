// @flow
import React from 'react';
import { View, Text, StyleSheet, ScrollView, FlatList, TouchableOpacity, TextInput } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import _ from 'lodash';
import { getFont, WIDTH, HEIGHT, popupOk } from '../../configs';
import NavigationService from '../../routers/NavigationService';
import Header from '../../common/Header/Header'
import PickerSearch from './PickerSearch';
import PickerItem from './PickerItem';
import PickerDate from './PickerDate';
import R from '../../assets/R';

const listField = [
  {
    tieuDe: 'Đơn vị',
    loai: 0,
    isRequire: true,
    data: R.strings.DON_VI,
  },
  {
    tieuDe: 'Phòng ban',
    loai: 0,
    isRequire: true,
    data: R.strings.PHONG_BAN
  },
  {
    tieuDe: 'Loại chứng từ',
    loai: 3,
    isRequire: false,
    data: []
  },
  {
    tieuDe: 'Loại tờ trình',
    loai: 1,
    isRequire: true,
    data: R.strings.LOAI_TO_TRINH
  },
  {
    tieuDe: 'Người yêu cầu',
    loai: 1,
    isRequire: true,
    data: R.strings.NGUOI_YEU_CAU
  },
  {
    tieuDe: 'Phòng/Ban kiểm soát chi phí',
    loai: 0,
    isRequire: true,
    data: R.strings.PHONG_BAN_KIEM_SOAT_CP
  },
  {
    tieuDe: 'Số chứng từ',
    loai: 2,
    isRequire: true,
    data: []
  },
  {
    tieuDe: 'Ngày lập',
    loai: 4,
    isRequire: true,
    data: []
  },
]

type Props = {
}

type State = {
  donVi: string,
  phongBan: string,
  chiPhi: string,
  chungTu: string,
  nguoiYc: string,
  taikhoan: string,
  checked: boolean
}

export default class PheDuyetDon extends React.Component<Props, State> {
    state={
      donVi: '',
      phongBan: '',
      chiPhi: '',
      chungTu: '',
      nguoiYc: '',
      taikhoan: '',
      checked: false
    }

    renderItem = (item: Object, index: number) => (
      <>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={[styles.title, item.loai < 2 && { color: R.colors.colorsBlue }]}>
            {_.has(item, 'tieuDe') ? item.tieuDe : ''}
          </Text>
          <Text style={{ paddingLeft: WIDTH(5), color: item.isRequire ? 'red' : R.colors.white }}>*</Text>
        </View>
        {
          index === 0
          && _.has(item, 'data')
          && <PickerSearch
            data={item.data}
            title={_.has(item, 'tieuDe') ? item.tieuDe : ''}
            onFill={(text) => { this.setState({ donVi: text }) }}
            checkRequire={this.state.checked}
            content={this.state.donVi}
          />
        }
        {
          index === 1
          && _.has(item, 'data')
          && <PickerSearch
            data={item.data}
            title={_.has(item, 'tieuDe') ? item.tieuDe : ''}
            onFill={(text) => { this.setState({ phongBan: text }) }}
            checkRequire={this.state.checked}
            content={this.state.phongBan}
          />
        }
        {
          index === 2
          && <TextInput
            style={styles.inputBox}
            onChangeText={taikhoan => this.setState({ taikhoan })}
            placeholder={_.has(item, 'tieuDe') ? item.tieuDe : ''}
          />
        }
        {(index === 3) && _.has(item, 'data') && <PickerItem data={item.data} />}
        {
          index === 4
          && _.has(item, 'data')
          && <PickerSearch
            title={_.has(item, 'tieuDe') ? item.tieuDe : ''}
            onFill={(text) => { this.setState({ nguoiYc: text }) }}
            data={item.data}
            checkRequire={this.state.checked}
            content={this.state.nguoiYc}
          />
        }
        {
          index === 5
          && _.has(item, 'data')
          && <PickerSearch
            title={_.has(item, 'tieuDe') ? item.tieuDe : ''}
            onFill={(text) => { this.setState({ chiPhi: text }) }}
            data={item.data}
            checkRequire={this.state.checked}
            content={this.state.chiPhi}
          />
        }
        {
          index === 6
          && _.has(item, 'data')
          && <TextInput
            style={styles.inputBox}
            placeholderTextColor={this.state.chungTu.length === 0 && this.state.checked ? 'red' : 'gray'}
            onChangeText={chungTu => this.setState({ chungTu })}
            placeholder={_.has(item, 'tieuDe') ? item.tieuDe : ''}
          />
        }
        {index === 7 && <PickerDate />}
      </>
    )

      onSubmit = () => {
        let { donVi, phongBan, chiPhi, chungTu, } = this.state
        if (donVi.length !== 0 && phongBan.length !== 0 && chiPhi.length !== 0 && chungTu.length !== 0) {
          console.log('Page 1', this.state)
          NavigationService.navigate('TaoToTrinhPage2')
        } else {
          this.setState({ checked: true })
          popupOk('Thông báo', 'Vui lòng điền đầy đủ thông tin.')
        }
      }

      render() {
        return (
          <View style={styles.container}>
            <Header title="Thêm tờ trình" number={1} />

            <ScrollView style={{ flex: 1 }}>
              <View style={{ width: WIDTH(375), paddingHorizontal: WIDTH(18), alignItems: 'center' }}>

                <View style={styles.body}>
                  <FlatList
                    data={listField}
                    style={{ paddingBottom: HEIGHT(5) }}
                    renderItem={({ item, index }) => this.renderItem(item, index)}
                    keyExtractor={({ item, index }) => index}
                  />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: WIDTH(347), }}>
                  <View />
                  <TouchableOpacity
                    style={styles.buttonNext}
                    onPress={() => { this.onSubmit() }}
                  >
                    <Text style={styles.textBtn}>Tiếp tục</Text>
                    <FontAwesome name="angle-double-right" color={R.colors.colorMain} size={HEIGHT(25)} />
                  </TouchableOpacity>
                </View>

              </View>
            </ScrollView>
          </View>

        )
      }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.white,
  },
  image: {
    height: HEIGHT(139),
    width: WIDTH(200),
    marginTop: HEIGHT(76),
    marginBottom: HEIGHT(77)
  },
  body: {
    width: WIDTH(339),
    paddingBottom: HEIGHT(5)
  },
  inputBox: {
    width: WIDTH(339),
    paddingVertical: HEIGHT(8),
    borderRadius: HEIGHT(8),
    height: HEIGHT(48),
    paddingHorizontal: WIDTH(14),
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: R.colors.strickColor,
  },
  inputBoxMk: {
    width: WIDTH(339),
  },
  title: {
    color: R.colors.textblack,
    fontSize: getFont(16),
    fontWeight: 'bold',
    marginBottom: HEIGHT(8),
    marginTop: HEIGHT(12)
  },
  quenMk: {
    fontSize: getFont(15),
    color: R.colors.textColor,
    textAlign: 'center',
    textDecorationColor: R.colors.textColor,
    textDecorationLine: 'underline',
    marginTop: HEIGHT(14)
  },
  textTouchId: {
    fontWeight: '500',
    fontSize: getFont(15),
    color: R.colors.colorMain,
    textAlign: 'center',
    marginTop: HEIGHT(16),
    marginBottom: HEIGHT(20)
  },
  buttonNext: {
    paddingVertical: HEIGHT(6),
    paddingHorizontal: WIDTH(14),
    backgroundColor: R.colors.white,
    borderRadius: HEIGHT(100),
    borderColor: R.colors.colorMain,
    borderWidth: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: HEIGHT(20)
  },
  textBtn: {
    fontWeight: '500',
    color: R.colors.colorMain,
    fontSize: getFont(15),
    marginRight: WIDTH(11)
  },
  imgTouch: {
    height: HEIGHT(48),
    width: WIDTH(48),
    marginTop: HEIGHT(64),
  }
})
