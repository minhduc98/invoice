// @flow
import React from 'react';
import {
  View,
  Picker,
  StyleSheet
} from 'react-native';
import _ from 'lodash';
import { HEIGHT, WIDTH } from '../../configs';
import R from '../../assets/R';

type Props = {
  isFilter: Function,
  width: number,
  data: Array<{
    id: string,
    name: string,
    value: string
  }>,
  value: string | number
}

type State = {
  language: number
}

export default class Thuycute extends React.Component<Props, State> {
  static defaultProps = {
    isFilter: () => {},
    width: WIDTH(338),
    value: 0
  }

  state ={
    language: 0,
  }

  render() {
    const { width, data, isFilter, value } = this.props
    return (
      <View style={[styles.inputBox, width && { width }]}>
        <Picker
          mode="dropdown"
          selectedValue={value || this.state.language}
          style={{ width: (width - WIDTH(10)) || WIDTH(328), height: HEIGHT(48), }}
          onValueChange={(itemValue, itemIndex) => {
            if (isFilter !== undefined) {
              isFilter(itemIndex)
            }
            this.setState({ language: itemValue })
          }
  }
        >
          {
              !_.isUndefined(data) && data.map((item, index) => (<Picker.Item key={index} label={item.name} value={index} />))
            }
        </Picker>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  inputBox: {
    width: WIDTH(339),
    borderRadius: HEIGHT(8),
    paddingHorizontal: WIDTH(14),
    justifyContent: 'center',

    borderWidth: 1,
    borderColor: R.colors.strickColor,
    flexDirection: 'row'
  },
})
