// @flow
import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DatePicker from 'react-native-datepicker'
import { WIDTH, HEIGHT, getFont } from '../../configs';
import R from '../../assets/R';

type State = {
  films: Array<{
    title: string
  }>,
  query: string,
  date: Date | string
}

type Props = {
  width: number
}

class AutocompleteExample extends Component<Props, State> {
  static defaultProps = {
    width: WIDTH(338)
  }

  constructor(props: Props) {
    super(props);
    this.state = {
      films: [{ title: 'Viettel' }, { title: 'Mobifonte' }],
      query: '',
      date: new Date()
    };
  }

  onChangeDate = (date: string) => {
    this.setState({ date })
  }

  componentDidMount() {
  }

  render() {
    const { width } = this.props
    return (
      <View style={[styles.inputBox, width && { width, marginRight: WIDTH(11) }]}>
        <DatePicker
          locale="vi"
          style={{ paddingHorizontal: 0, borderWidth: 0, width: (width - WIDTH(20)) || WIDTH(310), }}
          date={this.state.date}
          mode="date"
          placeholder="select date"
          format="DD/MM/YYYY"
          git
          maxDate="31/12"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          iconComponent={
            <View>
              <MaterialCommunityIcons
                size={WIDTH(22)}
                color={R.colors.blueGrey101}
                name="calendar-month"
              />
            </View>
      }
          customStyles={{
            dateInput: {
              marginLeft: WIDTH(0),
              borderWidth: 0,
              alignItems: 'flex-start'
            },
            dateText: {
              ...styles.textDate,
              marginLeft: 0
            }
          }}
          onDateChange={(date) => { this.onChangeDate(date) }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputBox: {
    width: WIDTH(339),
    borderRadius: HEIGHT(8),
    paddingHorizontal: WIDTH(14),
    height: HEIGHT(48),
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: R.colors.strickColor,
    flexDirection: 'row',
    alignItems: 'center'
  },
  textDate: {
    fontSize: getFont(16),
    color: R.colors.textColor
  }
});

export default AutocompleteExample;
