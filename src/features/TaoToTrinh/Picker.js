// @flow
import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { CheckBox } from 'react-native-elements'
import { WIDTH, HEIGHT, getFont } from '../../configs';
import R from '../../assets/R';

type State = {
  films: Array<{
    title: string
  }>,
  query: string,
  checked: boolean
}

type Props = {
}

class AutocompleteExample extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      films: [{ title: 'Viettel' }, { title: 'Mobifonte' }],
      query: '',
      checked: true
    };
  }

  onChangeBox = () => {
    this.setState({ checked: !this.state.checked })
  }

  componentDidMount() {
  }

  render() {
    return (
      <View style={styles.inputBox}>
        <CheckBox
          onPress={this.onChangeBox}
          containerStyle={{ backgroundColor: 'transparent', borderWidth: 0 }}
          center
          title="title"
          size={WIDTH(30)}
          iconLeft
          iconType="material"
          checkedIcon="check-box"
          uncheckedIcon="check-box-outline-blank"
          checkedColor={R.colors.colorMain}
          uncheckedColor={R.colors.blueGrey101}
          checked={this.state.checked}
          textStyle={styles.textDate}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputBox: {
    width: WIDTH(339),
    paddingVertical: HEIGHT(2),
    borderRadius: HEIGHT(8),
    paddingHorizontal: WIDTH(14),
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },
  textDate: {
    fontSize: getFont(16),
    color: R.colors.textColor
  }
});

export default AutocompleteExample;
