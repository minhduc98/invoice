// @flow
import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { CheckBox } from 'react-native-elements'
import { WIDTH, HEIGHT, getFont } from '../../configs';
import R from '../../assets/R';

type Props = {
  title: string
}

type State = {
  checked: boolean
}

class AutocompleteExample extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      checked: true
    };
  }

  onChangeBox = () => {
    this.setState({ checked: !this.state.checked })
  }

  componentDidMount() {
  }

  render() {
    const { title } = this.props
    return (
      <View style={styles.inputBox}>
        <CheckBox
          onPress={this.onChangeBox}
          containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, marginLeft: -WIDTH(10) }}
          center
          title={title}
          size={WIDTH(30)}
          iconLeft
          iconType="material"
          checkedIcon="check-box"
          uncheckedIcon="check-box-outline-blank"
          checkedColor={R.colors.colorMain}
          uncheckedColor={R.colors.blueGrey101}
          checked={this.state.checked}
          textStyle={styles.textDate}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputBox: {
    width: WIDTH(339),
    borderRadius: HEIGHT(8),
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: -HEIGHT(20)
  },
  textDate: {
    fontSize: getFont(16),
    color: R.colors.textColor,
    fontWeight: 'normal'
  }
});

export default AutocompleteExample;
