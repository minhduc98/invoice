// @flow
import React from 'react';
import { View, Text, StyleSheet, ScrollView, FlatList, TouchableOpacity, TextInput } from 'react-native';
import _ from 'lodash';
import { getFont, WIDTH, HEIGHT, popupOk } from '../../configs';
import NavigationService from '../../routers/NavigationService';
import CheckBox from './CheckBox';
import Header from '../../common/Header/Header'
import PickerSearch from './PickerSearch';
import PickerItem from './PickerItem';
import PickerDate from './PickerDate';
import R from '../../assets/R';

const listField = [
  {
    tieuDe: 'Trạng thái tài liệu',
    loai: 1,
    isRequire: false,
    data: R.strings.TRANG_THAI_TAI_LIEU
  },
  {
    tieuDe: 'Trạng thái duyệt',
    loai: 1,
    isRequire: false,
    data: R.strings.TRANG_THAI_DUYET
  },
  {
    tieuDe: 'Người tạo',
    loai: 3,
    isRequire: false,
    data: []
  },
  {
    tieuDe: 'Người cập nhật',
    loai: 3,
    isRequire: false,
    data: []
  },
  {
    tieuDe: 'Trạng thái ký',
    loai: 1,
    isRequire: false,
    data: R.strings.TRANG_THAI_KY
  },
  {
    tieuDe: 'Bản trình ký',
    loai: 5,
    isRequire: false,
    data: []
  },
  {
    tieuDe: 'Mã hiệu văn bản',
    loai: 3,
    isRequire: false,
    data: []
  },
]

type Props = {
}

type State = {
  taikhoan: string
}
export default class PheDuyetDon extends React.Component<Props, State> {
    state={
      taikhoan: ''
    }

    onSubmit = () => {
      popupOk('Thông báo', 'Tạo tờ trình thành công')
      console.log('Page 3', this.state)
      NavigationService.navigate('PheDuyetDon')
    }

    renderItem = (item: Object, index: number) => (
      <View>
        {
          _.has(item, 'loai')
          && item.loai !== 5
          && <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text style={[styles.title, item.loai < 2 && { color: R.colors.colorsBlue }]}>
              {_.has(item, 'tieuDe') ? item.tieuDe : ''}
            </Text>
            <Text style={{ paddingLeft: WIDTH(5), color: (_.has(item, 'isRequire') && item.isRequire) ? 'red' : R.colors.white }}>*</Text>
          </View>
        }
        {
          _.has(item, 'loai')
          && _.has(item, 'data')
          && item.loai === 0
          && <PickerSearch
            title={_.has(item, 'tieuDe') ? item.tieuDe : ''}
            data={item.data}
          />
        }
        {_.has(item, 'loai') && _.has(item, 'data') && item.loai === 1 && <PickerItem data={item.data} />}
        {_.has(item, 'loai') && item.loai === 4 && <PickerDate />}
        {_.has(item, 'loai') && item.loai === 5 && <CheckBox title={_.has(item, 'tieuDe') ? item.tieuDe : ''} />}
        {
          _.has(item, 'loai')
          && (item.loai === 2 || item.loai === 3)
          && <TextInput
            style={[styles.inputBox, _.has(item, 'tieuDe') && item.tieuDe === 'Nội dung' && { color: R.colors.teal501 }]}
            multiline={_.has(item, 'tieuDe') && item.tieuDe === 'Nội dung'}
            onChangeText={taikhoan => this.setState({ taikhoan })}
            placeholder={_.has(item, 'tieuDe') ? item.tieuDe : ''}
          />
        }
      </View>
    )

    render() {
      return (
        <View style={styles.container}>
          <Header title="Thêm tờ trình" number={3} />
          <ScrollView style={{ flex: 1 }}>
            <View style={{ width: WIDTH(375), paddingHorizontal: WIDTH(18), alignItems: 'center' }}>
              <View style={styles.body}>
                <FlatList
                  data={listField}
                  renderItem={({ item, index }) => this.renderItem(item, index)}
                  keyExtractor={({ item, index }) => index}
                />
              </View>
            </View>
            <View style={{ alignItems: 'center' }}>
              <TouchableOpacity
                onPress={this.onSubmit}
                style={styles.buttonLogin}
              >
                <Text style={styles.textBtnTao}>Tạo tờ trình</Text>
              </TouchableOpacity>
              {/* <Text style={styles.kiemtra}>Kiểm tra lại</Text> */}
            </View>
          </ScrollView>
        </View>
      )
    }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.white,
  },
  body: {
    width: WIDTH(339)
  },
  inputBox: {
    width: WIDTH(339),
    paddingVertical: HEIGHT(8),
    borderRadius: HEIGHT(8),
    height: HEIGHT(48),
    paddingHorizontal: WIDTH(14),
    fontSize: getFont(16),
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: R.colors.strickColor,
  },
  buttonLogin: {
    width: WIDTH(251),
    paddingVertical: HEIGHT(14),
    backgroundColor: R.colors.colorMain,
    borderRadius: HEIGHT(8),
    marginTop: HEIGHT(14),
    alignItems: 'center',
    justifyContent: 'center',
  },
  kiemtra: {
    fontSize: getFont(16),
    color: R.colors.teal501,
    textAlign: 'center',
    marginTop: HEIGHT(14)
  },
  textBtnTao: {
    fontWeight: '500',
    color: R.colors.white,
    fontSize: getFont(16),
    textAlign: 'center'
  },
  title: {
    color: R.colors.textblack,
    fontSize: getFont(16),
    fontWeight: 'bold',
    marginBottom: HEIGHT(8),
    marginTop: HEIGHT(12)
  },
  textBtn: {
    fontWeight: '500',
    color: R.colors.colorMain,
    fontSize: getFont(15),
    marginRight: WIDTH(11)
  },
})
