// @flow
import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { getFont, WIDTH, HEIGHT, popupOk } from '../../configs';
import NavigationService from '../../routers/NavigationService';
import R from '../../assets/R';

type State = {
  taikhoan: string,
  matkhau: string,
  iconEye: boolean,
  bottom: number,
  heightImage: number
}

type Props = {
}

export default class DangNhap extends React.Component<Props, State> {
    keyboardWillShowListener: Object;

    constructor(props: Props) {
      super(props);
      this.state = {
        taikhoan: '',
        matkhau: '',
        iconEye: true,
        bottom: 0,
        heightImage: 0
      }
      this.keyboardWillShowListener;
    }

    componentWillMount() {
      this.keyboardWillShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardWillShow);
      this.keyboardWillShowListener = Keyboard.addListener('keyboardDidHide', this._keyboardWillHide);
    }

    componentWillUnmount() {
      this.keyboardWillShowListener.remove();
      this.keyboardWillShowListener.remove();
    }

    _keyboardWillShow = () => {
      this.setState({
        bottom: HEIGHT(80),
        heightImage: HEIGHT(80),
      });
    };

    onSubmit =() => {
      const { taikhoan, matkhau } = this.state
      if (taikhoan.length !== 0 && matkhau.length !== 0) {
        NavigationService.navigate('Home1')
      } else popupOk('Thông báo', 'Vui lòng nhập đầy đủ thông tin.')
    }

      _keyboardWillHide = () => {
        this.setState({
          bottom: HEIGHT(80),
          heightImage: HEIGHT(80),
        });
      };

      render() {
        const { taikhoan, matkhau } = this.state
        return (
          <TouchableWithoutFeedback
            style={{ flex: 1, }}
            onPress={() => Keyboard.dismiss()}
          >

            <ScrollView>
              <View style={{ width: WIDTH(375), paddingHorizontal: WIDTH(18), alignItems: 'center' }}>
                <StatusBar backgroundColor={R.colors.white} />
                <Image resizeMode="stretch" style={styles.image} source={R.images.Logo} />
                <View style={styles.body}>
                  <Text style={styles.title}>Tài khoản</Text>
                  <TextInput
                    maxLength={30}
                    style={styles.inputBox}
                    onChangeText={taikhoan => this.setState({ taikhoan })}
                    value={taikhoan}
                  />
                  <Text style={styles.title}>Mật khẩu</Text>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TextInput
                      maxLength={30}
                      secureTextEntry={this.state.iconEye}
                      style={[styles.inputBox, { paddingRight: WIDTH(35) }]}
                      onChangeText={matkhau => this.setState({ matkhau })}
                      value={matkhau}
                    />
                    <TouchableOpacity
                      onPress={() => { this.setState({ iconEye: !this.state.iconEye }) }}
                      style={{ marginLeft: -WIDTH(30), zIndex: 10, marginTop: -HEIGHT(15) }}
                    >
                      <Ionicons name={this.state.iconEye ? 'ios-eye-off' : 'ios-eye'} color={R.colors.colorTextDetail} size={HEIGHT(25)} />
                    </TouchableOpacity>
                  </View>

                  <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity
                      onPress={this.onSubmit}
                      style={styles.buttonLogin}
                    >
                      <Text style={styles.textBtn}>Đăng nhập</Text>
                    </TouchableOpacity>
                    <Text style={styles.quenMk}>Quên mật khẩu?</Text>
                    <TouchableOpacity
                      onLongPress={() => { NavigationService.navigate('Home1') }}
                    >
                      <Image resizeMode="stretch" style={styles.imgTouch} source={R.images.VanTay} />
                    </TouchableOpacity>
                    <Text style={styles.textTouchId}>Đăng nhập bằng Touch ID</Text>
                  </View>

                </View>
              </View>
            </ScrollView>
          </TouchableWithoutFeedback>
        )
      }
}
const styles = StyleSheet.create({
  image: {
    height: HEIGHT(139),
    width: WIDTH(200),
    marginTop: HEIGHT(76),
    marginBottom: HEIGHT(77)
  },
  body: {
    width: WIDTH(339)
  },
  inputBox: {
    width: WIDTH(339),
    paddingVertical: HEIGHT(12),
    borderRadius: HEIGHT(8),
    paddingHorizontal: WIDTH(14),
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: R.colors.strickColor,
    marginBottom: HEIGHT(20)
  },
  inputBoxMk: {
    width: WIDTH(339),
  },
  title: {
    color: R.colors.textblack,
    fontSize: getFont(16),
    fontWeight: 'bold',
    marginBottom: HEIGHT(8)
  },
  quenMk: {
    fontSize: getFont(15),
    color: R.colors.textColor,
    textAlign: 'center',
    textDecorationColor: R.colors.textColor,
    textDecorationLine: 'underline',
    marginTop: HEIGHT(14)
  },
  textTouchId: {
    fontWeight: '500',
    fontSize: getFont(15),
    color: R.colors.colorMain,
    textAlign: 'center',
    marginTop: HEIGHT(16),
    marginBottom: HEIGHT(20)
  },
  buttonLogin: {
    width: WIDTH(251),
    paddingVertical: HEIGHT(14),
    backgroundColor: R.colors.colorMain,
    borderRadius: HEIGHT(8),
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBtn: {
    fontWeight: '500',
    color: R.colors.white,
    fontSize: getFont(16)
  },
  imgTouch: {
    height: HEIGHT(48),
    width: WIDTH(48),
    marginTop: HEIGHT(64),
  }
})
