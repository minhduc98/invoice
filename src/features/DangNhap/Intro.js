// @flow
import { Image, StatusBar, View } from 'react-native';
import * as React from 'react';
import * as Animatable from 'react-native-animatable';
// import config
import styles from './styles';
import R from '../../assets/R'

type Props = {
  navigation: Object
}

export class Intro extends React.PureComponent<Props> {
  changeScreen: Function

  constructor(props: Props) {
    super(props);
    this.changeScreen = this.changeScreen.bind(this);
  }

  async componentDidMount() {
    this.changeScreen('DangNhaps');
  }

  changeScreen(routeName: string) {
    const { dispatch } = this.props.navigation;
    setTimeout(() => {
      dispatch({
        type: 'Navigation/RESET',
        actions: [{
          type: 'Navigate',
          routeName
        }],
        index: 0
      });
    }, 3500);
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={R.colors.white} />
        <Animatable.View
          animation="bounceIn"
          direction="alternate"
          duration={4000}
          style={styles.logoContainer}
        >
          <Image
            resizeMode="contain"
            source={R.images.Logo}
            style={styles.image}
          />
        </Animatable.View>
      </View>
    );
  }
}
export default Intro
