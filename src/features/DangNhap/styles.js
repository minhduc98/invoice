// @flow
import { StyleSheet, } from 'react-native';

import { getFont, WIDTH, HEIGHT } from '../../configs';
import R from '../../assets/R';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoContainer: Object.assign(
    { justifyContent: 'center', alignItems: 'center' }
  ),
  image: {
    width: WIDTH(179),
    height: HEIGHT(133),
  },
  label: Object.assign(
    { justifyContent: 'center', alignItems: 'center' }
  ),
  textLabel: {
    color: R.colors.colorMain,
    fontSize: getFont(16),
    fontFamily: R.fonts.Roboto,
    fontWeight: 'bold'
  }
});

export default styles;
