// @flow
import React from 'react';
import { View, Text, StyleSheet, ScrollView, FlatList, Platform, UIManager, LayoutAnimation, TouchableOpacity } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { CheckBox } from 'react-native-elements';
import _ from 'lodash';
import { getFont, WIDTH, HEIGHT, getLineHeight, popupOk } from '../../configs';
import R from '../../assets/R';
import NavigationService from '../../routers/NavigationService';
import Header from '../../common/Header/Header'

type State = {
  expanded: boolean
}

type Props = {
  navigation: Object
}

export default class PheDuyetToTrinh extends React.Component<Props, State> {
  listField: Array<{
    tieuDe: string,
    loai: number,
    isRequire: boolean,
    data: Array<Object>,
    value: string,
    isShow: boolean
  }>

  constructor(props: Props) {
    super(props);
    this.state = {
      expanded: false
    }
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    let item = this.props.navigation.getParam('item')
    this.listField = [
      {
        tieuDe: 'Đơn vị',
        loai: 1,
        isRequire: true,
        data: [],
        value: 'Viettel Hà Nội',
        isShow: true
      },
      {
        tieuDe: 'Phòng ban',
        loai: 1,
        isRequire: true,
        data: [],
        value: `018390 - ${item.nguoiTao}`,
        isShow: true
      },
      {
        tieuDe: 'Loại chứng từ',
        loai: 2,
        isRequire: false,
        data: [],
        value: 'Tờ trình',
        isShow: false
      },
      {
        tieuDe: 'Loại tờ trình',
        loai: 1,
        isRequire: true,
        data: [],
        value: 'TT03 - Tờ trình chủ trương',
        isShow: true

      },
      {
        tieuDe: 'Người yêu cầu',
        loai: 1,
        isRequire: true,
        data: [],
        value: `HĐ: 018390 ${item.nguoiTao}`,
        isShow: true
      },
      {
        tieuDe: 'Phòng/Ban kiểm soát chi phí',
        loai: 1,
        isRequire: true,
        data: [],
        value: `018390 - ${item.nguoiTao}`,
        isShow: true
      },
      {
        tieuDe: 'Số chứng từ',
        loai: 2,
        isRequire: true,
        data: [],
        value: 'HNI.14.00TT170127',
        isShow: true
      },
      {
        tieuDe: 'Ngày lập',
        loai: 2,
        isRequire: true,
        data: [],
        value: '07/02/2020',
        isShow: false
      },
      {
        tieuDe: 'Số văn bản VOffice',
        loai: 2,
        isRequire: false,
        data: [],
        value: '15000002150/10275/TTr-KHDN',
        isShow: false
      },
      {
        tieuDe: 'Ngày duyệt VOffice',
        loai: 2,
        isRequire: false,
        data: [],
        value: '09/02/2020',
        isShow: false
      },
      {
        tieuDe: 'Nội dung',
        loai: 4,
        isRequire: true,
        data: [],
        value: item.tieuDe,
        isShow: true
      },
      {
        tieuDe: 'Tổng tiền đề nghị',
        loai: 2,
        isRequire: false,
        data: [],
        value: item.tongTien,
        isShow: false
      },
      {
        tieuDe: 'Tổng tiền được duyệt',
        loai: 2,
        isRequire: false,
        data: [],
        value: item.tongTien,
        isShow: false
      },
      {
        tieuDe: 'Chi các hoạt động tài trợ, quỹ phúc lợi',
        loai: 3,
        isRequire: false,
        data: [],
        value: '',
        isShow: false
      },
      {
        tieuDe: 'Tờ trình cha',
        loai: 1,
        isRequire: false,
        data: [],
        value: 'Viettel Hà Nội',
        isShow: false
      },
      {
        tieuDe: 'Đã kết thúc',
        loai: 3,
        isRequire: false,
        data: [],
        value: '',
        isShow: false
      },
      {
        tieuDe: 'Trạng thái tài liệu',
        loai: 2,
        isRequire: false,
        data: [],
        value: 'Hoàn thành',
        isShow: false
      },
      {
        tieuDe: 'Trạng thái duyệt',
        loai: 2,
        isRequire: false,
        data: [],
        value: 'Chưa được duyệt',
        isShow: false
      },
      {
        tieuDe: 'Người tạo',
        loai: 2,
        isRequire: false,
        data: [],
        value: `018390 - ${item.nguoiTao}`,
        isShow: false
      },
      {
        tieuDe: 'Người cập nhật',
        loai: 2,
        isRequire: false,
        data: [],
        value: '074351 - Trần Thị Út',
        isShow: false
      },
      {
        tieuDe: 'Trạng thái ký',
        loai: 2,
        isRequire: false,
        data: [],
        value: 'Hoàn thành',
        isShow: false
      },
      {
        tieuDe: 'Bản ghi trình ký',
        loai: 3,
        isRequire: false,
        data: [],
        value: '',
        isShow: false
      },
      {
        tieuDe: 'Mã hiệu văn bản',
        loai: 2,
        isRequire: false,
        data: [],
        value: 'HA-123',
        isShow: false
      },
    ]
  }

  changeLayout = () => {
    LayoutAnimation.configureNext(
      {
        duration: 400,
        create: {
          type: LayoutAnimation.Types.spring,
          property: LayoutAnimation.Properties.scaleY,
          springDamping: 1.7,
        },
        update: {
          type: LayoutAnimation.Types.spring,
          springDamping: 1.7,
        },
      }
    );
    this.setState({ expanded: !this.state.expanded });
  }

  renderItem = (item: Object, index: number) => {
    // if (index === 0) Alert.alert(this.state.expanded.toString())
    if (_.has(item, 'isShow') && item.isShow) {
      return (
        <View>
          {
            _.has(item, 'loai')
            && item.loai !== 3
            && <View>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={[styles.title, item.loai < 2 && { color: R.colors.colorsBlue }]}>
                  {_.has(item, 'tieuDe') ? item.tieuDe : ''}
                </Text>
                <Text style={{ paddingLeft: WIDTH(5), color: (_.has(item, 'isRequire') && item.isRequire) ? 'red' : R.colors.white }}>*</Text>
              </View>
              {
                (item.loai === 1 || item.loai === 2 || item.loai === 4)
                && <View>
                  <Text style={item.loai === 4 ? styles.inputBox2 : styles.inputBox}>{_.has(item, 'value') ? item.value : ''}</Text>
                </View>
              }
            </View>
          }
          {
            _.has(item, 'loai')
            && item.loai === 3
            && <CheckBox
              activeOpacity={1}
              checkedIcon="check-square"
              checked={_.has(item, 'value') ? item.value : ''}
              checkedColor={R.colors.colorMain}
              containerStyle={{ padding: 0, margin: 0, backgroundColor: R.colors.white, borderWidth: 0 }}
              title={_.has(item, 'tieuDe') ? item.tieuDe : ''}
              textStyle={{ fontFamily: R.fonts.Roboto, fontSize: getFont(15), color: R.colors.black1a1, fontWeight: 'normal' }}
              size={HEIGHT(24)}
            />
          }
        </View>
      )
    } else if (_.has(item, 'isShow') && !item.isShow) {
      return (
        <View style={{ height: this.state.expanded ? null : 0 }}>
          {
            _.has(item, 'loai')
            && item.loai !== 3
            && <View>
              <View style={{ flexDirection: 'row', alignItems: 'center', height: this.state.expanded ? null : 0 }}>
                <Text style={[styles.title, { height: this.state.expanded ? null : 0 }, item.loai < 2 && { color: R.colors.colorsBlue }]}>
                  {_.has(item, 'tieuDe') ? item.tieuDe : ''}
                </Text>
                <Text style={{ paddingLeft: WIDTH(5), color: (_.has(item, 'isRequire') && item.isRequire) ? 'red' : R.colors.white, height: this.state.expanded ? null : 0 }}>*</Text>
              </View>
              {
                (item.loai === 1 || item.loai === 2 || item.loai === 4)
                && <View style={{ height: this.state.expanded ? null : 0 }}>
                  <Text style={[item.loai === 4 ? styles.inputBox2 : styles.inputBox, { height: this.state.expanded ? null : 0, paddingVertical: this.state.expanded ? HEIGHT(15) : 0, marginBottom: this.state.expanded ? HEIGHT(20) : 0, borderWidth: this.state.expanded ? 1 : 0 }]}>{_.has(item, 'value') ? item.value : ''}</Text>
                </View>
              }
            </View>
          }
          {
            _.has(item, 'loai')
            && item.loai === 3
            && this.state.expanded
            && <CheckBox
              activeOpacity={1}
              checkedIcon="check-square"
              checked={_.has(item, 'value') ? item.value : ''}
              checkedColor={R.colors.colorMain}
              containerStyle={{ padding: 0, margin: 0, backgroundColor: R.colors.white, borderWidth: 0 }}
              title={_.has(item, 'tieuDe') ? item.tieuDe : ''}
              textStyle={{ fontFamily: R.fonts.Roboto, fontSize: getFont(15), color: R.colors.black1a1, fontWeight: 'normal' }}
              size={HEIGHT(24)}
            />
          }
        </View>
      )
    }
  }

  render() {
    return (

      <View style={styles.container}>
        <Header title="Phê duyệt tờ trình" />
        <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
          <View style={styles.scrollView1}>
            <View style={styles.body}>
              <FlatList
                data={this.listField}
                renderItem={({ item, index }) => this.renderItem(item, index)}
                keyExtractor={(item, index) => item + index}
                extraData={this.state}
              />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'center', width: WIDTH(360), }}>
              <TouchableOpacity style={styles.buttonNext} onPress={this.changeLayout}>
                <FontAwesome name={!this.state.expanded ? 'angle-double-down' : 'angle-double-up'} color={R.colors.colorMain} size={HEIGHT(35)} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.btBottom}>
            <TouchableOpacity
              style={{ paddingHorizontal: WIDTH(50), paddingVertical: HEIGHT(18), borderWidth: 1, borderRadius: HEIGHT(8), borderColor: R.colors.readBorder }}
              onPress={() => { popupOk('Thông báo', 'Đã từ chối tờ trình.'); NavigationService.pop() }}
            >
              <Text style={{ fontWeight: 'bold', fontFamily: R.fonts.Roboto, fontSize: getFont(16), lineHeight: getLineHeight(24), color: R.colors.readBorder }}>Từ chối</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ paddingHorizontal: WIDTH(50), paddingVertical: HEIGHT(19), borderRadius: HEIGHT(8), backgroundColor: R.colors.colorMain }}
              onPress={() => { popupOk('Thông báo', 'Đã phê duyệt tờ trình.'); NavigationService.pop() }}
            >
              <Text style={{ fontWeight: 'bold', fontFamily: R.fonts.Roboto, fontSize: getFont(16), lineHeight: getLineHeight(24), color: R.colors.white }}>Phê duyệt</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  btBottom: {
    paddingHorizontal: WIDTH(18),
    backgroundColor: R.colors.colorBackGroundMain,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: HEIGHT(16)
  },
  scrollView1: {
    width: WIDTH(375),
    paddingHorizontal: WIDTH(18),
    alignItems: 'center',
    borderBottomLeftRadius: HEIGHT(15),
    borderBottomRightRadius: HEIGHT(15),
    backgroundColor: R.colors.white,
    shadowColor: R.colors.black0,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  container: {
    flex: 1,
    backgroundColor: R.colors.colorBackGroundMain,
  },
  image: {
    height: HEIGHT(139),
    width: WIDTH(200),
    marginTop: HEIGHT(76),
    marginBottom: HEIGHT(77)
  },
  body: {
    width: WIDTH(339)
  },
  inputBox: {
    width: WIDTH(339),
    paddingVertical: HEIGHT(12),
    borderRadius: HEIGHT(8),
    paddingHorizontal: WIDTH(14),
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: R.colors.strickColor,
    marginBottom: HEIGHT(5),
    color: R.colors.textColor
  },
  inputBox2: {
    width: WIDTH(339),
    paddingVertical: HEIGHT(12),
    borderRadius: HEIGHT(8),
    paddingHorizontal: WIDTH(14),
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: R.colors.strickColor,
    marginBottom: HEIGHT(5),
    color: R.colors.colorMain,
    fontSize: getFont(16),
    fontWeight: 'bold',
    lineHeight: getLineHeight(24),
    fontFamily: R.fonts.Roboto
  },
  inputBoxMk: {
    width: WIDTH(339),
  },
  title: {
    color: R.colors.textblack,
    fontSize: getFont(16),
    fontWeight: 'bold',
    marginBottom: HEIGHT(6),
    marginTop: HEIGHT(8)
  },
  quenMk: {
    fontSize: getFont(15),
    color: R.colors.textColor,
    textAlign: 'center',
    textDecorationColor: R.colors.textColor,
    textDecorationLine: 'underline',
    marginTop: HEIGHT(14)
  },
  textTouchId: {
    fontWeight: '500',
    fontSize: getFont(15),
    color: R.colors.colorMain,
    textAlign: 'center',
    marginTop: HEIGHT(16),
    marginBottom: HEIGHT(20)
  },
  buttonNext: {
    paddingTop: HEIGHT(14),
    paddingHorizontal: WIDTH(14),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: HEIGHT(8)
  },
  textBtn: {
    fontWeight: '500',
    color: R.colors.colorMain,
    fontSize: getFont(15),
    marginRight: WIDTH(11)
  },
  imgTouch: {
    height: HEIGHT(48),
    width: WIDTH(48),
    marginTop: HEIGHT(64),
  }
})
