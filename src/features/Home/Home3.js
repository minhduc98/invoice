import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, PermissionsAndroid } from 'react-native'
import XLSX from 'xlsx';
import FileViewer from 'react-native-file-viewer';
import { popupOk } from '../../configs';


let RNFS = require('react-native-fs');

const fakeData = [
  {
    id: 'Abc123',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: 'Lớp trưởng',
    sdt: '0123456789',
  },
  {
    id: 'Abc124',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '0123456789',
  },
  {
    id: 'Abc125',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: 'Lớp phó',
    sdt: '0123456789',
  },
  {
    id: 'Abc126',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '0123456789',
  },
  {
    id: 'Abc127',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '',
  },
  {
    id: 'Abc123',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: 'Lớp trưởng',
    sdt: '0123456789',
  },
  {
    id: 'Abc124',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '0123456789',
  },
  {
    id: 'Abc125',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: 'Lớp phó',
    sdt: '0123456789',
  },
  {
    id: 'Abc126',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '0123456789',
  },
  {
    id: 'Abc127',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '',
  },
  {
    id: 'Abc123',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: 'Lớp trưởng',
    sdt: '0123456789',
  },
  {
    id: 'Abc124',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '0123456789',
  },
  {
    id: 'Abc125',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: 'Lớp phó',
    sdt: '0123456789',
  },
  {
    id: 'Abc126',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '0123456789',
  },
  {
    id: 'Abc127',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '',
  },
  {
    id: 'Abc123',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: 'Lớp trưởng',
    sdt: '0123456789',
  },
  {
    id: 'Abc124',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '0123456789',
  },
  {
    id: 'Abc125',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: 'Lớp phó',
    sdt: '0123456789',
  },
  {
    id: 'Abc126',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '0123456789',
  },
  {
    id: 'Abc127',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '',
  },
  {
    id: 'Abc123',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: 'Lớp trưởng',
    sdt: '0123456789',
  },
  {
    id: 'Abc124',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '0123456789',
  },
  {
    id: 'Abc125',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: 'Lớp phó',
    sdt: '0123456789',
  },
  {
    id: 'Abc126',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '0123456789',
  },
  {
    id: 'Abc127',
    name: 'Nguyễn Văn A',
    age: 19,
    address: 'Hà Đông, Hà Nội',
    chucVu: '',
    sdt: '',
  },
];

let path = `${RNFS.ExternalStorageDirectoryPath}/ExcelTest.xlsx`;
let ws = XLSX.utils.json_to_sheet(fakeData);
let wb = XLSX.utils.book_new();
XLSX.utils.book_append_sheet(wb, ws, 'SinhVien');
let str = XLSX.write(wb, { bookType: 'xlsx', type: 'base64' });


class Home3 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  requestStoragePermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.onExportFile();
      } else {
        // denied
      }
    } catch (err) {
    }
  }

  onExportFile = async () => {
    let isExist = await RNFS.exists(path);
    if (!isExist) {
      await RNFS.writeFile(path, str, 'base64')
        .then(success => {
          console.log('FILE WRITTEN!');
        })
        .catch(err => {
        });
    }
    this.onViewInExcel();
  };

  onViewInExcel = () => {
    FileViewer.open(path)
      .then(() => {
        // success
      })
      .catch(error => {
        // error
        popupOk('Thông báo', 'Thiết bị của bạn cần phải cài đặt ứng dụng đọc file excel.');
      })
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.requestStoragePermission}>
          <Text>View in Excel</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default Home3;
