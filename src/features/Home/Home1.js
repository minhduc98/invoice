import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Header from '../../common/Header/HeaderProfile';
import { HEIGHT, WIDTH } from '../../configs/Function';
import images from '../../assets/images';
import R from '../../assets/R';
import i18n, { setLocation } from '../../assets/i18n';
import styles from './stylesHome1';
import NavigationService from '../../routers/NavigationService';

class Home1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [
        {
          name: i18n.t('STATEMENT'),
          show: false,
          height: HEIGHT(60),
        },
        {
          name: i18n.t('OFFER_PAYMENT2'),
          show: false,
          height: HEIGHT(60),
        },
        {
          name: i18n.t('LIST_STATEMENT'),
          show: false,
          height: HEIGHT(60),
        }
      ]
    };
    this.menu = [
      {
        name: i18n.t('STATEMENT'),
        img: images.itemMenu1
      },
      {
        name: i18n.t('OFFER_PAYMENT'),
        img: images.itemMenu2
      },
      {
        name: i18n.t('LIST_STATEMENT'),
        img: images.itemMenu3
      },
      {
        name: i18n.t('BILL'),
        img: images.itemMenu4
      }
    ];
  }

  componentWillMount=() => {
    setLocation(i18n, 'vi')
  }

  onChangeLayout = (item, index) => {
    if (!item.show) {
      this.state.menu[index].show = true;
      this.state.menu[index].height = HEIGHT(225);
      this.setState({ menu: this.state.menu })
    } else if (item.show) {
      this.state.menu[index].show = false;
      this.state.menu[index].height = HEIGHT(60);
      this.setState({ menu: this.state.menu })
    }
  }

  renderItem2 = ({ item, index }) => (
    <TouchableOpacity
      onPress={() => {
        (index === 0) ? this.onChangeLayout(item, index) : null
      }}
      activeOpacity={1}
      style={[styles.itemContainer2, {
        borderTopWidth: (index === 0) ? WIDTH(1) : 0,
        height: item.height
      }]}
    >
      <View style={styles.dashboardMenu}>
        <Text style={styles.nameItem}>{item.name}</Text>
        {
        !item.show
          ? <Icon name="ios-arrow-forward" size={25} color={R.colors.blueGrey210} />
          : <Icon name="ios-arrow-down" size={25} color={R.colors.blueGrey210} />
        }
      </View>
      {
        !item.show
          ? <View />
          : (
            index === 0 && <View style={{ alignSelf: 'center' }}>
              <View style={styles.line2} />
              <Image source={images.circleFake} style={styles.imgCircle} resizeMode="contain" />
            </View>
          )
        }
    </TouchableOpacity>
  )

  renderItem = ({ item, index }) => (
    <TouchableOpacity
      style={styles.itemContainer}
      onPress={() => this.goToOtherScreenMenu(index)}
    >
      <Image source={item.img} style={styles.img} resizeMode="contain" />
      <Text style={[styles.nameItem, { marginTop: HEIGHT(8) }]}>{item.name}</Text>
    </TouchableOpacity>
  )

    goToOtherScreenMenu = (index) => {
      switch (index) {
        case 0: {
          NavigationService.navigate('PheDuyetDon')
          break;
        }
        default:
          break;
      }
    }

    goToHome2 = () => {
      NavigationService.navigate('Home2')
    }

    goToHome3 = () => {
      NavigationService.navigate('Home3')
    }

    goToProfile = () => {
      NavigationService.navigate('Profile')
    }

    render() {
      return (
        <View style={styles.container}>
          <Header fullName="Nguyễn Văn A" phongBan="Nhân viên kinh doanh" onPress={this.goToProfile} />
          <Text style={styles.label}>{i18n.t('FAVORITE')}</Text>
          <View style={styles.flatList}>
            <FlatList
              data={this.menu}
              renderItem={this.renderItem}
              horizontal
            />
          </View>
          <TouchableOpacity
            style={[styles.menuAll, { marginLeft: WIDTH(18) }]}
            onPress={this.goToHome2}
          >
            <Image source={images.all} style={styles.img} resizeMode="contain" />
            <Text style={[styles.nameItem, { marginTop: HEIGHT(5) }]}>{i18n.t('ALL')}</Text>
          </TouchableOpacity>
          <View style={styles.line} />
          <Text style={styles.label}>{i18n.t('DASHBOARD')}</Text>
          <View style={{ marginTop: HEIGHT(16) }}>
            <FlatList
              extraData={this.state}
              data={this.state.menu}
              renderItem={this.renderItem2}
            />
          </View>
          <View style={styles.btnFooter}>
            <TouchableOpacity>
              <Image source={images.btnHome} style={styles.img} resizeMode="contain" />
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flex: 0 }}
              onPress={this.goToHome2}
            >
              <MaterialIcons name="select-all" size={WIDTH(36)} color="#00918D" />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.goToHome3}>
              <Image source={images.btnCam} style={styles.img} resizeMode="contain" />
            </TouchableOpacity>
          </View>
        </View>
      );
    }
}

export default Home1;
