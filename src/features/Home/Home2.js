import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { HEIGHT, getLineHeight, getFont, WIDTH } from '../../configs/Function';
import R from '../../assets/R';
import Header from '../../common/Header/HeaderProfile';
import i18n from '../../assets/i18n';
import ComponentHome2 from '../../common/commonHome/componentHome2';
import NavigationService from '../../routers/NavigationService';

class Home2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.data = [
      {
        name: i18n.t('STATEMENT'),
        img: R.images.itemMenu1,
        onPress: this.ping
      },
      {
        name: i18n.t('LIST_STATEMENT'),
        img: R.images.itemMenu2,
        onPress: this.ping
      },
      {
        name: i18n.t('OFFER_PAYMENT'),
        img: R.images.itemMenu3,
        onPress: this.ping
      },
      {
        name: i18n.t('BILL'),
        img: R.images.itemMenu4,
        onPress: this.ping
      },
      {
        name: i18n.t('BILL2'),
        img: R.images.itemMenu4,
        onPress: this.ping
      },
    ]
    this.data2 = [
      {
        name: i18n.t('STATEMENT'),
        img: R.images.itemMenu1,
        onPress: this.ping
      },
      {
        name: i18n.t('LIST_STATEMENT'),
        img: R.images.itemMenu2,
        onPress: this.ping
      },
      {
        name: i18n.t('OFFER_PAYMENT'),
        img: R.images.itemMenu3,
        onPress: this.ping
      },
      {
        name: i18n.t('BILL'),
        img: R.images.itemMenu4,
        onPress: this.ping
      },
    ]
  }

  ping =() => alert('ping')

  goToOtherScreenMenu = (index) => {
    switch (index) {
      case 0: {
        NavigationService.navigate('PheDuyetDon')
        break;
      }
      default:
        break;
    }
  }

  renderItem =({ item, index }) => (
    <View>
      <TouchableOpacity
        style={styles.viewItem}
        onPress={() => this.goToOtherScreenMenu(index)}
      >
        <Image
          source={item.img}
          resizeMode="stretch"
          style={{ width: WIDTH(35), height: HEIGHT(36) }}
        />
        <Text style={styles.title}>{item.name}</Text>
      </TouchableOpacity>
    </View>
  )

  goToProfile = () => {
    NavigationService.navigate('Profile')
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header fullName="Nguyễn Văn A" phongBan="Nhân viên kinh doanh" onPress={this.goToProfile} />
        <ComponentHome2
          title={i18n.t('FAVORITE')}
          styleView={styles.viewContainer}
          styleTitle={styles.headTitle}
          data={this.data}
          renderItem={this.renderItem}
        />
        <View style={styles.line} />
        <ComponentHome2
          title={i18n.t('USE_RECENT')}
          styleView={styles.viewContainer2}
          styleTitle={styles.headTitle}
          data={this.data2}
          renderItem={this.renderItem}
        />
        <View style={styles.line} />
        <ComponentHome2
          title={i18n.t('ALL')}
          styleView={styles.viewContainer2}
          styleTitle={styles.headTitle}
          data={this.data2}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

export default Home2;
const styles = StyleSheet.create({
  headTitle: {
    fontWeight: '500',
    fontSize: getFont(18),
    lineHeight: getLineHeight(26),
    marginLeft: WIDTH(18),
    color: R.colors.black0,
  },
  line: {
    width: '100%',
    height: HEIGHT(8),
    backgroundColor: R.colors.strickColor
  },
  title: {
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.black0,
    marginTop: HEIGHT(8)
  },
  viewContainer: {
    height: HEIGHT(240),
    backgroundColor: R.colors.white100,
    paddingVertical: HEIGHT(16)
  },
  viewContainer2: {
    backgroundColor: R.colors.white100,
    paddingVertical: HEIGHT(16)
  },
  viewItem: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: WIDTH(95),
    marginTop: HEIGHT(18)
  },
});
