import { StyleSheet } from 'react-native';
import { HEIGHT, WIDTH, getFont, getLineHeight } from '../../configs/Function';
import R from '../../assets/R';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  label: {
    fontFamily: 'Roboto',
    fontSize: getFont(18),
    lineHeight: getLineHeight(26),
    fontWeight: 'bold',
    color: R.colors.black0,
    marginTop: HEIGHT(16),
    marginLeft: WIDTH(18)
  },
  img: {
    width: WIDTH(36),
    height: HEIGHT(36),
    alignSelf: 'center'
  },
  nameItem: {
    fontFamily: 'Roboto',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    fontWeight: 'normal',
    color: R.colors.black0
  },
  flatList: {
    marginTop: HEIGHT(20),
    marginLeft: WIDTH(18),
  },
  itemContainer: {
    marginRight: WIDTH(22)
  },
  menuAll: {
    width: WIDTH(44),
    height: HEIGHT(68),
  },
  line: {
    width: WIDTH(375),
    height: HEIGHT(8),
    backgroundColor: R.colors.strickColor,
    marginTop: HEIGHT(16)
  },
  itemContainer2: {
    borderBottomWidth: WIDTH(1),
    borderTopColor: R.colors.strickColor,
    borderBottomColor: R.colors.strickColor,
    justifyContent: 'center'
  },
  btnFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: WIDTH(375),
    paddingHorizontal: WIDTH(56),
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  line2: {
    width: WIDTH(339),
    height: HEIGHT(1),
    alignSelf: 'center',
    marginTop: HEIGHT(18),
    backgroundColor: R.colors.strickColor
  },
  dashboardMenu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: WIDTH(375),
    paddingHorizontal: WIDTH(18)
  },
  imgCircle: {
    width: WIDTH(140),
    height: HEIGHT(140),
    alignSelf: 'center',
    marginTop: HEIGHT(12)
  }
});

export default styles;
