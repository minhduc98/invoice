/* eslint-disable react/prefer-stateless-function */
// @flow
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import i18n from '../assets/i18n';

type Props = {
}

type State = {
}

export default class Home extends Component<Props, State> {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>{i18n.t('language')}</Text>
      </View>
    );
  }
}
