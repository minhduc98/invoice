import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { getFont, WIDTH, HEIGHT, getLineHeight } from 'configs/Function';
import Ionicons from 'react-native-vector-icons/Ionicons'
import ItemProfile from './ItemProfile';
import HeaderProfile from '../../common/Header/HeaderProfile';
import i18n, { setLocation } from '../../assets/i18n';
import R from '../../assets/R';
import NavigationService from '../../routers/NavigationService';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        id: i18n.t('DATAID'),
        fullName: i18n.t('DATANAME'),
        donVi: i18n.t('DATADONVI'),
        phongBan: i18n.t('DATAPHONGBAN'),
        vaiTro: i18n.t('DATAVAITRO'),
      }
    };
  }

  componentDidMount() {
    setLocation(i18n, 'vi');
  }

  logOut = () => {
    NavigationService.navigate('Intro')
  }

  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <HeaderProfile fullName={this.state.data.fullName} phongBan={this.state.data.vaiTro} />
        <View style={styles.container}>
          <ItemProfile title={i18n.t('MANV')} content={this.state.data.id} icon={false} index={1} />
          <ItemProfile title={i18n.t('DONVI')} content={this.state.data.donVi} icon={true} />
          <ItemProfile title={i18n.t('PHONGBAN')} content={this.state.data.phongBan} icon={true} />
          <ItemProfile title={i18n.t('VAITRO')} content={this.state.data.vaiTro} icon={true} />
          <TouchableOpacity style={styles.logOut} onPress={this.logOut}>
            <Ionicons name="ios-log-out" color={R.colors.colorTextDetail} size={HEIGHT(25)} />
            <Text style={styles.text}>{i18n.t('LOG_OUT')}</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: HEIGHT(56),
    borderTopWidth: 0.5,
    borderColor: R.colors.strickColor,
  },
  text: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    marginLeft: WIDTH(13),
    color: R.colors.blue708,

  },
  logOut: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginRight: WIDTH(18),
    marginTop: HEIGHT(22),
  }
});

export default Profile;
