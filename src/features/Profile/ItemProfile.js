import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { getFont, WIDTH, HEIGHT, getLineHeight } from 'configs/Function';

class ItemProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  // eslint-disable-next-line class-methods-use-this
  renderItem(isShow) {
    if (isShow === true) { return (<AntDesign name="right" size={WIDTH(22)} color="#BAC7D3" style={{ marginRight: WIDTH(21) }} />) }
    return null;
  }

  render() {
    return (
      <View>
        <TouchableOpacity style={[styles.item, { borderToptWidth: (this.props.index === 1) ? 0.5 : 0 }]}>
          <Text style={styles.title}>
            {this.props.title}
            :
            {' '}
            <Text style={styles.content}>{this.props.content}</Text>
          </Text>
          {this.renderItem(this.props.icon)}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: HEIGHT(22),
    // height: HEIGHT(60),
    borderBottomWidth: 0.5,
    borderColor: '#D3DEE8',
  },
  title: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    marginLeft: WIDTH(18),
  },
  content: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
  }
});

export default ItemProfile;
