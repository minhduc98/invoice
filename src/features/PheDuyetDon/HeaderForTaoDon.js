// @flow
import React from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import { getFont, WIDTH, HEIGHT } from '../../configs';
import R from '../../assets/R';

const HeaderForTaoDon = () => (
  <View style={styles.container}>
    <StatusBar backgroundColor={R.colors.colorMain} />
    <Entypo name="menu" size={WIDTH(22)} color={R.colors.white} />
    <Text style={styles.textTitle}>Phê duyệt tờ trình</Text>
  </View>
)

export default HeaderForTaoDon;

const styles = StyleSheet.create({
  container: {
    minHeight: HEIGHT(44),
    paddingVertical: HEIGHT(10),
    backgroundColor: R.colors.colorMain,
    paddingRight: WIDTH(17),
    paddingLeft: WIDTH(19),
    flexDirection: 'row',
    alignItems: 'center'
  },
  textTitle: {
    fontSize: getFont(17),
    color: R.colors.white,
    fontWeight: '500',
    marginLeft: WIDTH(14)
  }
})
