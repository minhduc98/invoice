// @flow
import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import Octicons from 'react-native-vector-icons/Octicons';
import _ from 'lodash';
import { getFont, WIDTH, HEIGHT } from '../../configs';
import NavigationService from '../../routers/NavigationService';
import Header from '../../common/Header/Header'
import ItemAction from './ItemAction';
import LocTimKiem from './LocTimKiem';
import R from '../../assets/R';

const data = [
  {
    tieuDe: 'Thanh toán phí dịch vụ chữ kí số Viettel - CA phục vụ công việc tháng 02 - 03/2020',
    nguoiTao: 'Nguyễn Hải Lý',
    tongTien: '12.000.000'
  },
  {
    tieuDe: 'Thanh toán chi phí thực nghiệm mạng di động 5G đối với doanh nghiệp Hà Nội',
    nguoiTao: 'Hoàng Quân',
    tongTien: '50.000.000'
  },
  {
    tieuDe: 'Hỗ trợ triển khai dịch vụ internet đối với các chi nhánh Điện Biên và Lai Châu',
    nguoiTao: 'Lý Thu An',
    tongTien: '80.000.000'
  },
  {
    tieuDe: 'Thanh toán phí dịch vụ chữ kí số Viettel - CA phục vụ công việc tháng 06 - 07/2020',
    nguoiTao: 'Nguyễn Hải Lý',
    tongTien: '32.000.000'
  },
  {
    tieuDe: 'TThanh toán chi phí thực nghiệm mạng di động 5G đối với doanh nghiệp Đà Nẵng',
    nguoiTao: 'Mã Tuấn Anh',
    tongTien: '42.000.000'
  }

]
const dataLoc1 = [
  {
    tieuDe: 'Thanh toán phí dịch vụ chữ kí số Viettel - CA phục vụ công việc tháng 02 - 03/2020',
    nguoiTao: 'Nguyễn Hải Lý',
    tongTien: '12.000.000'
  },
  {
    tieuDe: 'Thanh toán phí dịch vụ chữ kí số Viettel - CA phục vụ công việc tháng 06 - 07/2020',
    nguoiTao: 'Nguyễn Hải Lý',
    tongTien: '32.000.000'
  },
  {
    tieuDe: 'TThanh toán chi phí thực nghiệm mạng di động 5G đối với doanh nghiệp Đà Nẵng',
    nguoiTao: 'Mã Tuấn Anh',
    tongTien: '42.000.000'
  }
]
const dataLoc2 = [
  {
    tieuDe: 'Thanh toán chi phí thực nghiệm mạng di động 5G đối với doanh nghiệp Hà Nội',
    nguoiTao: 'Hoàng Quân',
    tongTien: '50.000.000'
  },
  {
    tieuDe: 'Hỗ trợ triển khai dịch vụ internet đối với các chi nhánh Điện Biên và Lai Châu',
    nguoiTao: 'Lý Thu An',
    tongTien: '80.000.000'
  },
]

type State = {
  loading: boolean,
  data: Array<{
    tieuDe: string,
    nguoiTao: string,
    tongTien: string
  }>
}

type Props = {
}

export default class PheDuyetDon extends React.Component<Props, State> {
  LocTimKiem: Object

  constructor(props: Props) {
    super(props);
    this.state = {
      loading: false,
      data
    }
    this.LocTimKiem
  }

  openModal = () => {
    this.LocTimKiem.setModalVisible(false);
  }

  onChangeData = (loc: number) => {
    this.setState({ loading: true })
    setTimeout(() => {
      if (loc === 0) {
        this.setState({ loading: false, data })
      }
      if (loc === 1) {
        this.setState({ loading: false, data: dataLoc1 })
      }
      if (loc === 2) {
        this.setState({ loading: false, data: dataLoc2 })
      }
    }, 300)
  }

  renderItem = (item: Object, index: number) => (
    <TouchableOpacity
      style={styles.viewItem}
      onPress={() => NavigationService.navigate('PheDuyetToTrinh', { item })}
    >
      <View>
        <Text style={styles.textTenDo}>
          {_.has(item, 'tieuDe') ? item.tieuDe : ''}
        </Text>
        <View style={styles.itemBot}>
          <Text style={styles.tien}>
            {_.has(item, 'tongTien') ? `${item.tongTien} ` : '0 '}
            <Text style={styles.textVND}>VND</Text>
          </Text>
          <View style={[styles.itemBot, { marginLeft: WIDTH(14) }]}>
            <Octicons name="primitive-dot" color={R.colors.orangeA201} size={HEIGHT(10)} />
            <Text style={styles.ten}>
              {_.has(item, 'nguoiTao') ? item.nguoiTao : ''}
            </Text>
          </View>
        </View>
      </View>
      <Image source={R.images.IconRight} style={styles.IconRight} resizeMode="stretch" />
    </TouchableOpacity>
  )

  render() {
    return (
      <View style={styles.container}>
        <Header title="Phê duyệt tờ trình" />
        <ItemAction onLocTimKiem={() => { this.LocTimKiem.setModalVisible(true) }} />
        {this.state.loading ? <View style={{
          WIDTH: WIDTH(360),
          paddingTop: HEIGHT(200),
          justifyContent: 'center',
          alignItems: 'center',
        }}
        >
          <ActivityIndicator color={R.colors.black1a} animating size="large" />
        </View> : <View style={styles.body}>
                                <FlatList
            data={this.state.data}
            renderItem={({ item, index }) => this.renderItem(item, index)}
          />
                                        </View>}

        <LocTimKiem
          ref={ref => this.LocTimKiem = ref}
          onPressConfirm={this.onChangeData}
        />
      </View>

    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.whiteBgr,
  },
  body: {
    backgroundColor: R.colors.whiteBgr,
    width: WIDTH(375),
    paddingHorizontal: WIDTH(10),
    alignItems: 'center'
  },
  textTenDo: {
    fontSize: getFont(16),
    fontWeight: '500',
    color: R.colors.black0,
    width: WIDTH(295),
    flexWrap: 'wrap',
    marginBottom: HEIGHT(8)
  },
  tien: {
    color: R.colors.colorMain,
    fontWeight: 'bold',
    fontSize: getFont(24),

  },
  ten: {
    fontSize: getFont(16),
    color: R.colors.textColor,
    marginLeft: WIDTH(14)
  },
  viewItem: {
    width: WIDTH(355),
    minHeight: HEIGHT(106),
    paddingVertical: HEIGHT(10),
    paddingHorizontal: WIDTH(12),
    backgroundColor: R.colors.white,
    borderRadius: HEIGHT(8),
    marginTop: HEIGHT(14),
    flexDirection: 'row',
    alignItems: 'center',
    shadowOffset: { height: 2, width: 0 },
    shadowColor: R.colors.black10p,
    shadowOpacity: 0,
    elevation: 1
  },
  textVND: {
    fontSize: getFont(14),
    color: R.colors.colorMain,
    fontWeight: '500',
    marginRight: WIDTH(14)
  },
  itemBot: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  IconRight: {
    width: WIDTH(24),
    height: WIDTH(24),
    marginLeft: WIDTH(20)
  }
})
