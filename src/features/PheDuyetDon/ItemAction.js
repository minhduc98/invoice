// @flow
import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { getFont, WIDTH, HEIGHT } from '../../configs';
import NavigationService from '../../routers/NavigationService';
import R from '../../assets/R';

type Props = {
  onLocTimKiem: Function
}

const ItemAction = (props: Props) => (
  <View style={styles.container}>
    <TouchableOpacity
      onPress={() => { NavigationService.navigate('TaoToTrinh') }}
      style={styles.wrapperAdd}
    >
      <Image source={R.images.Add} style={styles.iconImage} />
      <Text style={styles.textTitle}>Thêm tờ trình</Text>
    </TouchableOpacity>
    <TouchableOpacity
      onPress={() => props.onLocTimKiem()}
      style={[styles.wrapperAdd, { borderColor: R.colors.colorMain, backgroundColor: R.colors.white }]}
    >
      <Image source={R.images.Filter} style={styles.iconImage} />
      <Text style={[styles.textTitle, { color: R.colors.colorMain }]}>Lọc tìm kiếm</Text>
    </TouchableOpacity>
  </View>
)

export default ItemAction;

const styles = StyleSheet.create({
  container: {
    minHeight: HEIGHT(44),
    paddingVertical: HEIGHT(10),
    backgroundColor: R.colors.white,
    paddingHorizontal: WIDTH(30),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  textTitle: {
    fontSize: getFont(16),
    color: R.colors.white,
    fontWeight: '500',
    marginLeft: WIDTH(10)
  },
  wrapperAdd: {
    minWidth: WIDTH(150),
    minHeight: HEIGHT(36),
    paddingVertical: HEIGHT(6),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: HEIGHT(100),
    flexDirection: 'row',
    backgroundColor: R.colors.colorMain,
    borderWidth: 1,
    borderColor: R.colors.colorMain,
  },
  iconImage: {
    height: WIDTH(24),
    width: WIDTH(24)
  }
})
