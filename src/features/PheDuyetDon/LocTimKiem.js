// @flow
import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, TouchableOpacity, FlatList, TextInput, TouchableWithoutFeedback } from 'react-native';
import _ from 'lodash';
import { getFont, HEIGHT, WIDTH } from '../../configs';
import R from '../../assets/R';
import PickerSearch from '../TaoToTrinh/PickerSearch';
import PickerItem from '../TaoToTrinh/PickerItem';
import PickerDate from '../TaoToTrinh/PickerDate'

const listField = [
  {
    tieuDe: 'Tổng tiền đề nghị',
    loai: 10,
    isRequire: false,
    data: R.strings.LOC_THEO_TONG_TIEN
  },
  {
    tieuDe: 'Loại tờ trình',
    loai: 1,
    isRequire: false,
    data: R.strings.LOAI_TO_TRINH
  },
  {
    tieuDe: 'Số tờ trình gốc',
    loai: 3,
    isRequire: false,
    data: []
  },
  {
    tieuDe: 'Trạng thái duyệt',
    loai: 1,
    isRequire: false,
    data: R.strings.TRANG_THAI_DUYET
  },
]
const condDate = [{
  tieuDe: 'Ngày lập gốc',
  loai: 1,
  isRequire: false,
  data: []
},
{
  tieuDe: 'Đến ngày',
  loai: 5,
  isRequire: false,
  data: []
},
]

type State = {
  modalVisible: boolean,
  tongTien: number,
  taikhoan: string
}

type Props = {
  onPressConfirm: Function
}

class LocTimKiem extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      modalVisible: false,
      tongTien: 0,
      taikhoan: ''
    };
  }

  setModalVisible = (visible: boolean) => {
    this.setState({
      modalVisible: visible
    })
  }

  renderItem = (item: Object, index: number) => (
    <View>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Text style={[styles.title]}>
          {_.has(item, 'tieuDe') ? item.tieuDe : ''}
        </Text>
      </View>
      {_.has(item, 'loai')
        && _.has(item, 'data')
        && item.loai === 0
        && <PickerSearch width={WIDTH(331)} data={item.data} />
      }
      {_.has(item, 'loai')
        && _.has(item, 'data')
        && item.loai === 1
        && <PickerItem width={WIDTH(331)} data={item.data} />
      }
      {_.has(item, 'loai')
        && _.has(item, 'data')
        && item.loai === 10
        && <PickerItem
          width={WIDTH(331)}
          data={item.data}
          value={this.state.tongTien}
          isFilter={(tongTien) => {
            this.setState({ tongTien })
          }}
        />
      }
      {_.has(item, 'loai')
        && _.has(item, 'tieuDe')
        && (item.loai === 2 || item.loai === 3)
        && <TextInput
          style={[styles.inputBox, item.tieuDe === 'Nội dung' && { color: R.colors.teal501 }]}
          multiline={item.tieuDe === 'Nội dung'}
          onChangeText={taikhoan => this.setState({ taikhoan })}
          placeholder={item.tieuDe}
        />
      }
    </View>
  )

  onSubmit=() => {
    this.setState({
      modalVisible: false
    })
  }

  renderItemDate = (item: Object) => (
    <View>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Text style={[styles.title]}>
          {_.has(item, 'tieuDe') ? item.tieuDe : ''}
        </Text>
      </View>
      <PickerDate width={WIDTH(160)} minDate="01/01/1900" />
    </View>
  )

  render() {
    const { onPressConfirm } = this.props
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
      >
        <TouchableWithoutFeedback
          onPress={() => { this.setModalVisible(false) }}
        >
          <View
            style={styles.opacity}
          >
            <TouchableWithoutFeedback>
              <View style={styles.modal}>
                <View style={{ alignItems: 'center' }}>

                  <View style={styles.body}>
                    <FlatList
                      data={condDate}
                      renderItem={({ item }) => this.renderItemDate(item)}
                      keyExtractor={({ item, index }) => index}
                      numColumns={2}
                    />
                    <FlatList
                      data={listField}
                      renderItem={({ item, index }) => this.renderItem(item, index)}
                      keyExtractor={({ item, index }) => index}
                    />
                  </View>
                </View>
                <View style={{ alignItems: 'center' }}>
                  <TouchableOpacity
                    onPress={() => {
                      onPressConfirm(this.state.tongTien)
                      this.setModalVisible(false)
                    }}
                    style={styles.buttonLogin}
                  >
                    <Text style={styles.textBtnTao}>Tìm kiếm</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      this.setModalVisible(false)
                    }}
                  >
                    <Text style={styles.kiemtra}>Thoát</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

export default LocTimKiem;
const styles = StyleSheet.create({
  opacity: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: R.colors.black70p
  },
  modal: {
    backgroundColor: R.colors.white100,
    width: WIDTH(355),
    borderRadius: WIDTH(10),
    paddingTop: HEIGHT(16),
    paddingBottom: HEIGHT(14),
    alignItems: 'center',
    paddingHorizontal: WIDTH(12)
  },
  body: {
    width: WIDTH(331)
  },
  inputBox: {
    width: WIDTH(331),
    paddingVertical: HEIGHT(8),
    borderRadius: HEIGHT(8),
    paddingHorizontal: WIDTH(14),
    height: HEIGHT(48),
    fontSize: getFont(16),
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: R.colors.strickColor,
  },
  buttonLogin: {
    width: WIDTH(251),
    paddingVertical: HEIGHT(14),
    backgroundColor: R.colors.colorMain,
    borderRadius: HEIGHT(8),
    marginTop: HEIGHT(14),
    alignItems: 'center',
    justifyContent: 'center',
  },
  kiemtra: {
    fontSize: getFont(16),
    color: R.colors.colorTextDetail,
    textAlign: 'center',
    marginTop: HEIGHT(14)
  },
  textBtnTao: {
    fontWeight: '500',
    color: R.colors.white,
    fontSize: getFont(16),
    textAlign: 'center'
  },
  title: {
    color: R.colors.textblack,
    fontSize: getFont(16),
    fontWeight: 'bold',
    marginBottom: HEIGHT(8),
    marginTop: HEIGHT(12)
  },
  textBtn: {
    fontWeight: '500',
    color: R.colors.colorMain,
    fontSize: getFont(15),
    marginRight: WIDTH(11)
  },
});
