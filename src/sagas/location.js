import { WATCH_CURRENT_LOCATION, UPDATE_CURRENT_LOCATION } from 'actions';
import { takeLatest, put } from 'redux-saga/effects';

function* handleNearPlaces(body) {
  yield put({
    type: UPDATE_CURRENT_LOCATION,
    body
  })
}

export function* watchNearPlaces() {
  yield takeLatest(WATCH_CURRENT_LOCATION, handleNearPlaces);
}
