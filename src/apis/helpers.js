/* eslint-disable no-console */
/* eslint-disable consistent-return */
/* eslint-disable no-return-await */
/**
 * helper.js - for storing reusable logic.
 */
import axios from 'axios';
import { getLangCode } from '../configs'
import { showAlert, TYPE } from '../common/DropdownAlert';
import AsyncStorageUtils from '../helpers/AsyncStorageUtils';
import NavigationService from '../routers/NavigationService';
import { LoginScreen } from '../routers/screenNames';
import { languages } from '../assets'
import env from '../env';
import { TOKEN_EXPIRED } from './Status'

const instance = axios.create({
  baseURL: `${env.serverURL}/api`,
  timeout: 15 * 1000,
});


export async function fetch(url, data, isAuth = false, token) {
  let headers = null;
  let langCode = getLangCode()
  if (isAuth) {
    // let token = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.TOKEN)
    headers = {
      Authorization: `Bearer ${token}`,
      langCode
    };
  } else {
    headers = { langCode }
  }

  return instance
    .get(url, {
      params: {
        ...data,
      },
      headers,
    })
    .then(response => {
      if (isAuth && response.data.status === TOKEN_EXPIRED) {
        showAlert(TYPE.ERROR, languages[langCode].sorry, languages[langCode].login_session_expired);
        AsyncStorageUtils.remove(AsyncStorageUtils.KEY.USER_DATA)
        NavigationService.reset(LoginScreen)
        return true
      } else {
        return response.data
      }
    })
    .catch(error => error);
}


export function fetchWithHeader(url, data, headers = {}) {
  return instance
    .get(url, {
      params: {
        ...data,
      },
      headers: {
        langCode: getLangCode(),
        ...headers,
      },
    })
    .then(response => response.data)
    .catch(error => error);
}

export async function post(url, data, isAuth = false) {
  let langCode = getLangCode()
  let headers = null;
  if (isAuth) {
    let token = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.TOKEN)
    headers = {
      Authorization: `Bearer ${token}`,
      langCode
    };
  } else {
    headers = { langCode }
  }
  return instance
    .post(
      url,
      { ...data },
      {
        headers,
      }
    )
    .then(response => {
      if (isAuth && response.data.status === TOKEN_EXPIRED) {
        showAlert(TYPE.ERROR, languages[langCode].sorry, languages[langCode].login_session_expired);
        AsyncStorageUtils.remove(AsyncStorageUtils.KEY.USER_DATA)
        NavigationService.reset(LoginScreen)
        return true
      } else {
        return response.data
      }
    })
    .catch(error => {
      console.log('error: ', error);
      return error
    });
}

export async function put(url, data, isAuth = false, token) {
  let langCode = getLangCode()
  let headers = null;
  if (isAuth) {
    // let token = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.TOKEN)
    headers = {
      Authorization: `Bearer ${token}`,
      langCode
    };
  } else {
    headers = { langCode }
  }
  return instance
    .put(
      url,
      { ...data },
      {
        headers,
      }
    )
    .then(response => {
      // console.log('=========================', response)
      if (isAuth && response.data.status === TOKEN_EXPIRED) {
        showAlert(TYPE.ERROR, languages[langCode].sorry, languages[langCode].login_session_expired);
        AsyncStorageUtils.remove(AsyncStorageUtils.KEY.USER_DATA)
        NavigationService.reset(LoginScreen)
        return true
      } else {
        return response.data
      }
    })
    .catch(error => error);
}

export async function del(url, data, isAuth = false) {
  let langCode = getLangCode()
  let headers = null;
  if (isAuth) {
    let token = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.TOKEN)
    headers = {
      Authorization: `Bearer ${token}`,
      langCode
    };
  } else {
    headers = { langCode }
  }
  return instance
    .delete(
      url,
      { ...data },
      {
        headers,
      }
    )
    .then(response => {
      if (isAuth && response.data.status === TOKEN_EXPIRED) {
        showAlert(TYPE.ERROR, languages[langCode].sorry, languages[langCode].login_session_expired);
        AsyncStorageUtils.remove(AsyncStorageUtils.KEY.USER_DATA)
        NavigationService.reset(LoginScreen)
        return true
      } else {
        return response.data
      }
    })
    .catch(error => error);
}

export async function postWithFormData(url, data, isAuth = false) {
  let langCode = getLangCode()
  let instance2 = axios.create({ baseURL: `${env.serverURL}/api` });
  let headers = null;
  if (isAuth) {
    let token = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.TOKEN)
    headers = {
      Authorization: `Bearer ${token}`,
      langCode
    };
  } else {
    headers = { langCode }
  }
  return instance2
    .post(
      url,
      data,
      {
        headers,
      }
    )
    .then(response => {
      if (isAuth && response.data.status === TOKEN_EXPIRED) {
        showAlert(TYPE.ERROR, languages[langCode].sorry, languages[langCode].login_session_expired);
        AsyncStorageUtils.remove(AsyncStorageUtils.KEY.USER_DATA)
        NavigationService.reset(LoginScreen)
        return true
      } else {
        return response.data
      }
    })
    .catch(error => error);
}

export async function uploadFiles(url, data, isAuth = false, onUploadProgress = null, cb) {
  let instance2 = axios.create({ baseURL: `${env.serverURL}/api` });
  let headers = null;
  let langCode = getLangCode()
  if (isAuth) {
    let token = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.TOKEN)
    headers = {
      Authorization: `Bearer ${token}`,
      langCode
    };
  } else {
    headers = { langCode }
  }

  const { CancelToken } = axios;

  return instance2
    .post(
      url,
      data,
      {
        headers,
        onUploadProgress,
        cancelToken: new CancelToken(((c) => { cb(c) }))
      }
    )
    .then(response => {
      if (isAuth && response.data.status === TOKEN_EXPIRED) {
        showAlert(TYPE.ERROR, languages[langCode].sorry, languages[langCode].login_session_expired);
        AsyncStorageUtils.remove(AsyncStorageUtils.KEY.USER_DATA)
        NavigationService.reset(LoginScreen)
        return true
      } else {
        return response.data
      }
    })
    .catch(error => {
      console.log('error: 1', error);
      return error
    });
}

export async function postFormData(url, body, isAuth = false) {
  let headers = null;
  if (isAuth) {
    let token = await AsyncStorageUtils.get(AsyncStorageUtils.KEY.TOKEN)
    headers = {
      Authorization: `Bearer ${token}`,
      langCode: getLangCode()
    };
  } else {
    headers = { langCode: getLangCode() }
  }
  let data = new FormData();
  Object.keys(body).forEach(key => {
    if (data[key] instanceof Array) {
      data[key].forEach(value => {
        data.append(`${key}[]`, value);
      });
    } else {
      data.append(key, data[key]);
    }
  });
  return instance
    .post(url, data, {
      headers,
    })
    .then(response => response.data)
    .catch(error => error);
}

export async function FetchJsonGet(url) {
  const myRequest = {
    method: 'get',
    url,
    headers: {
      'Content-Type': 'application/json'
    },
  };
  return await axios(myRequest)
    .then(response => {
      response.data
    })
    .then(responseJson => responseJson)
    .catch(error => {
      console.debug(error);
    });
}
export async function FetchDeleteWithTokenGet(url, accessToken, json) {
  const myRequest = {
    method: 'delete',
    url,
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'Content-Type': 'application/json'
    },
  };
  return await axios(myRequest)
    .then(response => response.data)
    .then(responseJson => responseJson)
    .catch(error => {
      console.debug(error);
    });
}

export async function FetchJsonWithTokenGet(url, accessToken) {
  const myRequest = {
    method: 'get',
    url,
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };
  return await axios(myRequest)
    .then(response => response.data)
    .then(responseJson => responseJson)
    .catch(error => {
      console.debug(error);
    });
}

export async function AxiosiWithTokenGet(url, accessToken) {
  const myRequest = {
    method: 'get',
    url,
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };
  return await axios(myRequest)
    .then(response => response.data)
    .then(responseJson => responseJson)
    .catch(error => {
      console.debug(error);
    });
}

export async function FetchFormDataWithToken(url, accessToken, json) {
  const myRequest = {
    method: 'post',
    url,
    headers: {
      Authorization: `Bearer ${accessToken}`,
      // Accept: "application/json",
      'Content-Type': 'multipart/form-data'
    },
    data: json
  };
  return await axios(myRequest)
    .then(response => {
      if (response.status === 200) {
        return response.data;
      } else {
        console.debug('Something went wrong on api server!');
      }
    })
    .then(response => response)
    .catch(error => {
      console.debug(error);
    });
}

export async function FetchJsonWithToken(url, accessToken, json) {
  const myRequest = {
    method: 'post',
    url,
    headers: {
      Authorization: `Bearer ${accessToken}`,
      // ccept: 'application/json',
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(json)
  };

  return await axios(myRequest)
    .then(response => {
      if (response.status === 200) {
        return response.data;
      } else {
        console.debug('Something went wrong on api server!');
      }
    })
    .then(response => response)
    .catch(error => {
      console.debug(error);
    });
}

export async function FetchJsonWithTokenPut(url, accessToken, json) {
  const myRequest = {
    method: 'put',
    url,
    headers: {
      Authorization: `Bearer ${accessToken}`,
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(json)
  };
  return await axios(myRequest)
    .then(response => {
      if (response.status === 200 || response.status === 500) {
        return response.data;
      } else {
        console.debug('Something went wrong on api server!');
      }
    })
    .then(response => response)
    .catch(error => {
      console.debug(error);
    });
}
export async function FetchAuthor(url, token, json) {
  const myRequest = {
    method: 'post',
    url,
    headers: {
      Authorization: token,
      // Accept: "application/json",
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(json)
  };
  return await axios(myRequest)
    .then(response => {
      if (response.status === 200 || response.status === 500) {
        return response.data;
      } else {
        console.debug('Something went wrong on api server!');
      }
    })
    .then(response => response)
    .catch(error => {
      console.debug(error);
    });
}
export async function FetchAuthorGet(url, token, json) {
  const myRequest = {
    method: 'get',
    url,
    headers: {
      Authorization: token,
      // Accept: "application/json",
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(json)
  };
  return await axios(myRequest)
    .then(response => {
      if (response.status === 200 || response.status === 500) {
        return response.data;
      } else {
        console.debug('Something went wrong on api server!');
      }
    })
    .then(response => response)
    .catch(error => {
      console.debug(error);
    });
}
export async function FetchJson(url, json) {
  const myRequest = {
    method: 'post',
    url,
    headers: {
      // Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(json)
  };
  return await axios(myRequest)
    .then(response => {
      if (response.status === 200) {
        return response.data;
      } else {
        console.debug('Something went wrong on api server!');
      }
    })
    .then(response => response)
    .catch(error => {
      console.debug(error);
    });
}
export function FetchJsonWait(list, json) {
  return new Promise((resolve, reject) => {
    let fetches = [];
    let arr = [];
    list.map(v => {
      const myRequest = new Request(v.link, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        data: JSON.stringify(json)
      });
      let { key_store } = v;
      fetches.push(
        fetch(myRequest)
          .then((data) => data.json())
          .then((dataJson) => {
            arr.push(new Data(key_store, dataJson.rows));
          })
      )
    });
    Promise.all(fetches).then(() => {
      resolve(arr);
    })
  });
}

export async function FetchAuthorWithFormData(url, token, json) {
  const myRequest = new Request(url, {
    method: 'POST',
    headers: {
      Authorization: token,
      // Accept: "application/json",
      'Content-Type': 'multipart/form-data'
    },
    data: json
  });
  return await axios(myRequest)
    .then(response => {
      if (response.status === 200) {
        return response.json();
      } else {
        console.debug('Something went wrong on api server!');
      }
    })
    .then(response => response)
    .catch(error => {
      console.debug(error);
    });
}
