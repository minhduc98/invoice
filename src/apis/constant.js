/**
 * constants.js - where all required static values are stored.
 */

// accounts
export const SIGNIN = 'signin';
export const REGISTER = 'register';
export const SIGNUP = 'signup';
