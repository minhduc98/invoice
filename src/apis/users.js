/* eslint-disable handle-callback-err */
import * as contants from './constant'
import { post, fetch } from './helpers'

export const getUserInfor = () => fetch(contants.MY_INFO, {}, true).then(res => res).catch(err => null);
export const ApiChangePassword = async (body) => post(contants.CHANGE_PASSWORD, body).then(res => res).catch(err => null);
