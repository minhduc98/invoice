// import * as firebase from 'firebase';
// import { InfectionStat, News, GeneralData } from './interface';

// const firebaseConfig = {
//   apiKey: 'AIzaSyDIXAquDINULzR8y1OApdEvxSwHZ5tJls8',
//   appId: '1:183773947398:web:b22076ba9ccd5a4f6e8587',
//   authDomain: 'the-wuhan-virus.firebaseapp.com',
//   databaseURL: 'https://the-wuhan-virus.firebaseio.com',
//   measurementId: 'G-3NL0SZNQXZ',
//   messagingSenderId: '183773947398',
//   projectId: 'the-wuhan-virus',
//   storageBucket: 'the-wuhan-virus.appspot.com',
// };

// firebase.initializeApp(firebaseConfig);

// const dataToDraw: any[] = [];
// // Lay tin tuc
// firebase.database().ref('/update_cards').once('value').then((snapshot: firebase.database.DataSnapshot) => {
//   const rawData = snapshot.val();
//   for (const i in rawData) {
//     // i = 0 la tieu de
//     if (Number(i) !== 0) {
//       const currentData = rawData[i];
//       const infectNews: News = {
//         title: currentData[0],
//         content: currentData[1],
//         source: currentData[2],
//         time_added_string: currentData[3],
//         time_added: currentData[4],
//         source_link: currentData[5],
//       };
//       console.log('INFECT NEWS', infectNews);
//     }
//   }
// });

// // Lay thong tin nhiem
// firebase.database().ref('/infections_intl').once('value').then((snapshot: firebase.database.DataSnapshot) => {
//   const rawData = snapshot.val();
//   // tslint:disable-next-line:forin
//   for (const i in rawData) {
//     // i = 0 la tieu de
//     // i = length - 1 la total
//     // i khac la country
//     if (Number(i) === 0) {
//       dataToDraw.push(['Country', 'Confirmed Cases']);
//     } else if (Number(i) === rawData.length - 1) {
//       const totalStat = rawData[i];
//       const infectTotalStat: InfectionStat = {
//         country: totalStat[0],
//         infected: totalStat[1],
//         deaths: totalStat[2],
//         cured: totalStat[3],
//         country_ch: totalStat[4],
//         country_jp: totalStat[5],
//         source_name: totalStat[6],
//         source_url: totalStat[7],
//       };
//       console.log('TOTAL STAT', infectTotalStat);
//     } else if (Number(i) !== 0) {
//       const countryStat = rawData[i];
//       const infectCountryStat: InfectionStat = {
//         country: countryStat[0],
//         infected: countryStat[1],
//         deaths: countryStat[2],
//         cured: countryStat[3],
//         country_ch: countryStat[4],
//         country_jp: countryStat[5],
//         source_name: countryStat[6],
//         source_url: countryStat[7],
//       };
//       console.log('INFECT COUNTRY', infectCountryStat);
//       // push data vào mảng để draw
//       const country = { v: countryStat[0].slice(0, -4), f: countryStat[0] };
//       const infectedNumber = { v: Math.log(countryStat[1]), f: countryStat[1] };
//       dataToDraw.push([country, infectedNumber]);
//     }
//   }
// });

// Lay thong tin chung
// firebase.database().ref('/general_data').once('value').then((snapshot: firebase.database.DataSnapshot) => {
//   const rawData = snapshot.val();

//   const generalData: GeneralData = {
//     infected_numbers: rawData[1][0],
//     deaths_numbers: rawData[1][1],
//     affected_countries_numbers: rawData[1][2],
//   };
//   console.log('GENERAL', generalData);
// });
