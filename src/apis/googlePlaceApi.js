import axios from 'axios';
import * as contants from './constant'

const instance = axios.create({
  baseURL: contants.GG_MAP_URL,
  timeout: 10 * 1000,
});

export async function fetch(url, data) {
  return instance
    .get(url, {
      params: {
        key: contants.GG_PLACE_API_KEY,
        fields: 'photos,formatted_address,name,rating,opening_hours,geometry',
        inputtype: 'textquery',
        ...data,
      },
    })
    .then(response => {
      console.log('response: ', response);
      return response.data
    })
    .catch(error => {
      console.log('error: ', error);
      return error
    });
}

export const googlePlaceByText = (data) => {
  return fetch(contants.GG_PLACE_URL, data, true)
    .then(res => res)
    .catch(err => null);
}