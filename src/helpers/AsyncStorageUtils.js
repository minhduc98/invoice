import AsyncStorage from '@react-native-community/async-storage';

const KEY = {
  LAST_TIME_SHOW_RATING_DIALOG: 'LAST_TIME_SHOW_RATING_DIALOG',
  CAN_SHOW_RATING_DIALOG: 'CAN_SHOW_RATING_DIALOG',
  LANG_CODE: 'LANG_CODE',
  REMEMBER_PASSWORD: 'REMEMBER_PASSWORD',
  USER: 'USER',
  USER_DATA: 'USER_DATA',
  TOKEN: 'TOKEN',
  ROLE: 'ROLE',
  IMAGE_UPLOADED: 'IMAGE_UPLOADED'
};

function save(key, value) {
  AsyncStorage.setItem(key, value);
}

// sử dụng nếu AsyncStorage dữ liệu dạng Json object
function saveObject(key, value) {
  AsyncStorage.setItem(key, JSON.stringify(value));
}

async function get(key) {
  return AsyncStorage.getItem(key);
}

async function remove(key) {
  return AsyncStorage.removeItem(key)
}

// sử dụng nếu AsyncStorage dữ liệu dạng Json object
async function getObject(key) {
  let value = await AsyncStorage.getItem(key)
  return JSON.parse(value)
}

export default {
  save,
  saveObject,
  get,
  getObject,
  remove,
  KEY,
};
