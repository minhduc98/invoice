import { Platform } from 'react-native';
import { USER_ROLE } from 'configs/constants'

let NODE_DEV = 'development';
let appType = USER_ROLE.TEACHER

// eslint-disable-next-line no-undef
if (__DEV__) {
  NODE_DEV = 'production';
} else {
  NODE_DEV = 'production';
}

console.log('Environment: ', NODE_DEV);

const serverURL = {
  // development: 'http://192.168.1.88:3000', // ip company
  development: 'http://192.168.1.177:3000', // ip company
  production: 'http://165.22.108.71:3000',
};

const socketURL = {
  development: 'http://165.22.108.71:8888', // ip company
  production: 'http://165.22.108.71:8888'
};

const codePushKey = {
  development: Platform.OS === 'ios' ? 'ios-key-development' : 'android-key-development',
  production: Platform.OS === 'ios' ? 'ios-key-development' : 'android-key-development',
};

const oneSignalKey = {
  development: '4fb93417-307d-4c62-80e2-0a78094af00',
  production: '4fb93417-307d-4c62-80e2-0a78094af00',
};

const iosGoogleClientId = {
  development: '677781444372-ih33jtssldk83p5geqts256m3jl2h0f0.apps.googleusercontent.com',
  production: '677781444372-ih33jtssldk83p5geqts256m3jl2h0f0.apps.googleusercontent.com',
}

const SENTRY_KEY = 'sentry-key-here';

export default {
  currentNode: NODE_DEV,
  serverURL: serverURL[NODE_DEV],
  socketURL: socketURL[NODE_DEV],
  codePushKey: codePushKey[NODE_DEV],
  oneSignalKey: oneSignalKey[NODE_DEV],
  iosGoogleClientId: iosGoogleClientId[NODE_DEV],
  sentryKey: SENTRY_KEY,
  appType
};
