import React from 'react'
import { TextInput, StyleSheet, TextInputProps } from 'react-native';
import R from '../assets/R';

export default class Input extends React.PureComponent<TextInputProps> {
  render() {
    return (
      <TextInput
        {...this.props}
        placeholder={this.props.placeholder || 'Enter something'}
        placeholderTextColor={this.props.placeholderTextColor || R.colors.grey501}
        keyboardType={this.props.keyboardType || 'default'}
        style={this.props.style || [style.inputView, this.props.customStyle || {}]}
      />
    )
  }
}
const style = StyleSheet.create({
  input: {
    padding: 0,
    paddingLeft: 8,
    fontSize: 14,
    backgroundColor: R.colors.white,
    flex: 1,
    opacity: 0.8
  },
  inputView: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    height: 35,
    borderBottomColor: R.colors.borderC,
    width: '80%',
    alignSelf: 'center',
    marginBottom: 8,
    paddingBottom: 0
  },
  w12: { width: 12 }
})
