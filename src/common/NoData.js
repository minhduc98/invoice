import React from 'react'
import { Text, StyleSheet } from 'react-native';
import R from '../assets/R';

type Props = {
  msg?: string
}
export default class NoData extends React.PureComponent<Props> {
  render() {
    return <Text style={styles.notFound}>{this.props.msg || 'Không có dữ liệu'}</Text>
  }
}
const styles = StyleSheet.create({
  notFound: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    padding: 20,
    color: R.colors.black9
  }
})
