import React, { PureComponent } from 'react';
import { View, TextInput, StyleSheet, TouchableOpacity, Image, Text } from 'react-native';
import { languages } from '../assets';
import R from '../assets/R';
import { getLangCode } from '../configs';
import NavigationService from '../routers/NavigationService';


class SearchView extends PureComponent {
  state = {
    clear: false,
    keyword: this.props.keyword || '',
    langCode: getLangCode()
  };

  static getDerivedStateFromProps(props, state) {
    if (props.keyword) return { keyword: props.keyword }
    return null
  }

  render() {
    let { onSearch } = this.props;
    let { clear, keyword, langCode } = this.state;
    return (
      <View style={styles.content}>
        <View style={styles.boxSearch}>
          <TouchableOpacity style={styles.btnSearch} onPress={onSearch}>
            <Image
              style={styles.icon}
              source={R.images.searchHeader}
            />
          </TouchableOpacity>

          <TextInput
            style={styles.txtSearch}
            value={keyword}
            returnKeyLabel="OK"
            onFocus={this.showBtnClose}
            onSubmitEditing={onSearch}
            onChangeText={this.onChangeText}
            placeholderTextColor={R.colors.gray}
            placeholder={languages[langCode].search}
            autoFocus
          />

          {clear && (
            <TouchableOpacity style={styles.btnClose} onPress={this.onClear}>
              <Image
                style={styles.iconClose}
                source={R.images.closeSearch}
              />
            </TouchableOpacity>
          )}
        </View>
        <TouchableOpacity
          onPress={NavigationService.pop}
        >
          <Text style={styles.txtCancel}>{languages[langCode].cancel}</Text>
        </TouchableOpacity>
      </View>

    );
  }

  onChangeText = keyword => {
    if (keyword.length === 0) {
      this.setState({ clear: false, keyword });
    } else {
      this.setState({ clear: true, keyword });
    }
  };

  showBtnClose = () => this.setState({ clear: true });

  onClear = () => {
    this.setState({ keyword: '', clear: false }, this.props.onClear || this.props.onSearch || null);
  };

  getValue = () => this.state.keyword;
}

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  iconStyle: {
    height: 16,
    width: 16,
    resizeMode: 'contain'
  },
  txt: {
    color: R.colors.white100,
    fontSize: 16,
    textAlign: 'center',
    fontWeight: '400'
  },
  icon: {
    width: 18,
    height: 18,
    resizeMode: 'contain'
  },
  iconClose: {
    width: 12,
    height: 12,
    resizeMode: 'contain'
  },
  btnSearch: {
    padding: 8,
    paddingLeft: 15,
    paddingRight: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnClose: {
    padding: 8,
    paddingLeft: 15,
    paddingRight: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconOutside: {
    tintColor: R.colors.primaryColor
  },
  flex: {
    flex: 1
  },
  cancel: {
    color: R.colors.white100,
    padding: 10
  },
  txtSearch: {
    flex: 1,
    color: R.colors.gray
  },
  txtCancel: {
    color: R.colors.primaryColor,
    fontSize: 15,
    padding: 10
  },
  txtOutside: {
    color: R.colors.primaryColor,
    flex: 1,
    alignSelf: 'center'
  },
  boxSearch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    borderRadius: 8,
    borderColor: R.colors.borderE,
    borderWidth: 1,
    height: 40,
    margin: 10
  }
});

export default SearchView;
