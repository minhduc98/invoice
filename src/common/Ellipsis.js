import React from 'react';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import R from '../assets/R';

export default class Ellipsis extends React.PureComponent {
  render() {
    let { onPress, show } = this.props;
    if (!show) return null

    let size = this.props.size || 7
    let color = this.props.color || R.colors.black5

    let iconStyle = [style.iconEllipsis, { width: size, height: size, borderRadius: size / 2, borderColor: color }]
    return (
      <TouchableOpacity
        onPress={onPress}
        style={style.ellipsisBtn}
      >
        <View style={iconStyle} />
        <View style={iconStyle} />
        <View style={iconStyle} />
      </TouchableOpacity>
    );
  }
}

const style = StyleSheet.create({
  ellipsisBtn: {
    paddingHorizontal: 0,
    paddingVertical: 5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  iconEllipsis: {
    borderWidth: 2.5,
    marginLeft: 2
  }
});
