import React from 'react'
import { View, StyleSheet, Picker } from 'react-native'
import { countries } from '../configs';
import R from '../assets/R';

export default class Countries extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      country: props.country || 'VN'
    }
  }

  static getDerivedStateFromProps(props) {
    console.log('props: ', props);
    if (!!props.country) {
      return { country: props.country }
    }
    return null
  }

  render() {
    let width = this.props.width || '85%'
    return (
      <View style={[style.inputView, { width }]}>
        <Picker
          selectedValue={this.state.country}
          style={style.picker}
          onValueChange={this.props.onChangeCountry}
        >
          {
            Object.keys(countries).map((e, i) => <Picker.Item label={countries[e]} value={e} key={i} />)
          }
        </Picker>
      </View>
    )
  }

  getValue = () => this.state.country
}

const style = StyleSheet.create({
  inputView: {
    borderWidth: 1,
    borderColor: R.colors.primaryColor,
    alignSelf: 'center',
    borderRadius: 19,
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 15,
    height: 38
  },
  picker: { height: 50, width: '100%' }
})
