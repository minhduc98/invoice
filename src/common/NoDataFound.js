import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { getLangCode } from '../configs';
import { languages } from '../assets';
import R from '../assets/R';

export default class NoDataFound extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    let langCode = getLangCode()
    let { title, content, icon, iconStyle } = this.props
    return (
      <View>
        <Image
          source={icon || R.images.noData}
          style={[styles.image, iconStyle || {}]}
        />
        <Text style={styles.txtBold}>{title || languages[langCode].no_data_available}</Text>
        <Text style={styles.txtNormal}>{content || languages[langCode].there_is_no_data_to_show_you_right_now}</Text>

        {this.props.children}
      </View>
    )
  }
}
const styles = StyleSheet.create({
  image: {
    alignSelf: 'center',
    marginBottom: 10,
    marginTop: 30,
    width: 80,
    height: 80,
    resizeMode: 'contain'
  },
  txtBold: {
    fontWeight: 'bold',
    color: R.colors.gray,
    fontSize: 16,
    textAlign: 'center'
  },
  txtNormal: {
    fontWeight: '400',
    color: R.colors.gray50,
    fontSize: 14,
    textAlign: 'center'
  }
})
