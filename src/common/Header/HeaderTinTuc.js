import React from 'react';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import { Badge } from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import NavigationService from '../../routers/NavigationService';
import { ListThongBao } from '../../routers/screenNames';
import { getFont, WIDTH, getLineHeight, HEIGHT } from '../../configs';
import R from '../../assets/R';


export default (props: any) => {
  let { title } = props
  return (
    <View style={{ paddingVertical: HEIGHT(8) }}>
      <StatusBar backgroundColor={R.colors.colorMain} />
      <View
        style={{
          backgroundColor: R.colors.colorBackGroundMain,
          width: WIDTH(360),
          height: HEIGHT(43),
          flexDirection: 'row',
          paddingHorizontal: WIDTH(15),
          justifyContent: 'space-between',
          zIndex: 1,
          alignItems: 'center'
        }}
      >
        <Text numberOfLines={1} style={styles.text}>
          {title}
        </Text>

        <TouchableOpacity
          onPress={() => {
            NavigationService.navigate(ListThongBao)
          }}
        >
          <Badge
            value={3}
            // ListNotiOfUser.newNotification > 5
            //   ? '5+'
            //   : ListNotiOfUser.newNotification
            containerStyle={{ position: 'absolute', top: -4, right: -8, width: WIDTH(25) }}
            badgeStyle={{ elevation: 2 }}
            status="success"
          />
          <MaterialCommunityIcons
            name="bell-circle"
            size={HEIGHT(32)}
            color={R.colors.colorMain}
          />
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  text: {
    color: R.colors.textblack,
    fontFamily: R.fonts.Roboto,
    width: WIDTH(250),
    flexWrap: 'wrap',
    fontSize: getFont(30),
    lineHeight: getLineHeight(36),
    fontWeight: 'bold',
  },
});
