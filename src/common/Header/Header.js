import React from 'react';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import NavigationService from '../../routers/NavigationService';
import { getFont, WIDTH, getLineHeight, HEIGHT } from '../../configs';
import R from '../../assets/R';

export default (props: any) => {
  let { title, number } = props
  return (
    <View style={{ paddingVertical: HEIGHT(10),
      paddingHorizontal: WIDTH(14),
      alignItems: 'center',
      flexDirection: 'row',
      backgroundColor: R.colors.colorMain,
      justifyContent: 'space-between'
    }}
    >
      <StatusBar backgroundColor={R.colors.colorMain} />
      <View style={{ alignItems: 'center',
        flexDirection: 'row', }}
      >
        <TouchableOpacity onPress={() => NavigationService.pop()}>
          <AntDesign name="arrowleft" size={HEIGHT(30)} color={R.colors.white} />
        </TouchableOpacity>
        <Text numberOfLines={1} style={styles.text}>
          {title}
          {number !== undefined && ` (${number} / 3)`}
        </Text>
      </View>
      {number > 1 && <TouchableOpacity
        onPress={() => NavigationService.navigate('PheDuyetDon')}
      >
        <Text style={{ color: R.colors.strickColor, fontSize: getFont(16), fontWeight: '500' }}>
          Thoát
        </Text>
                     </TouchableOpacity>
         }
    </View>
  )
}

const styles = StyleSheet.create({
  text: {
    color: R.colors.white,
    fontFamily: R.fonts.Roboto,
    width: WIDTH(225),
    flexWrap: 'wrap',
    fontSize: getFont(18),
    marginLeft: WIDTH(14),
    lineHeight: getLineHeight(24),
    fontWeight: '500',
  },
  imageIcon: {
    height: HEIGHT(30),
    width: WIDTH(30)
  }
});
