import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, StatusBar, Image, TouchableOpacity } from 'react-native';
import { getFont, WIDTH, HEIGHT, getLineHeight } from 'configs/Function';
import R from '../../assets/R';

export default class HeaderProfile extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={R.colors.colorMain} />
        <TouchableOpacity onPress={this.props.onPress}>
          <Image source={R.images.avatar} style={styles.img} />
        </TouchableOpacity>
        <View style={styles.textProfile}>
          <Text style={styles.textTitle}>{this.props.fullName}</Text>
          <Text style={styles.textDetail}>{this.props.phongBan}</Text>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    minHeight: HEIGHT(44),
    paddingVertical: HEIGHT(10),
    backgroundColor: R.colors.colorMain,
    paddingRight: WIDTH(17),
    paddingLeft: WIDTH(19),
    flexDirection: 'row',
    alignItems: 'center'
  },
  textTitle: {
    fontSize: getFont(18),
    lineHeight: getLineHeight(26),
    color: R.colors.white,
    fontWeight: '500',
    marginLeft: WIDTH(14)
  },
  textDetail: {
    fontSize: getFont(15),
    lineHeight: getLineHeight(22),
    color: R.colors.white,
    fontWeight: '500',
    marginLeft: WIDTH(14)
  },
  img: {
    width: WIDTH(50),
    height: WIDTH(50),
  },
  textProfile: {
    justifyContent: 'center',
    flexDirection: 'column',
  }
})
