import { StyleSheet, View, Image, Text } from 'react-native'
import React, { Component } from 'react'
import NetInfo from '@react-native-community/netinfo';
import { width, height } from '../configs'
import R from '../assets/R'

class NoInternetScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isConnected: true
    }
  }

  componentDidMount() {
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleConnectivityChange
    )
  }

  handleConnectivityChange = connected => {
    let { isConnected } = this.state
    if (isConnected !== connected) {
      this.setState({ isConnected: connected })
    }
  }

  render() {
    let { isConnected } = this.state
    if (!isConnected) {
      return (
        <View style={styles.offlineContainer}>
          <Image
            source={R.images.checknetworking}
            style={styles.imageStyle}
            resizeMode="contain"
          />
          <Text style={styles.textStyle}>Không có kết nối mạng</Text>
          <Text style={styles.textStyle}>Vui lòng kiểm tra lại!</Text>
        </View>
      )
    } else {
      return <View />
    }
  }
}

const styles = StyleSheet.create({
  offlineContainer: {
    flex: 1,
    backgroundColor: R.colors.white100,
    justifyContent: 'center',
    alignItems: 'center',
    width,
    height,
    position: 'absolute'
  },
  offlineText: { color: R.colors.white100 },
  textStyle: {
    fontSize: 16,
    color: R.colors.primaryColor
  },
  imageStyle: {
    width: width / 2,
    height: height / 3
  }
})

export default NoInternetScreen
