/* eslint-disable react/no-unused-state */
import React from 'react'
import { Image, ActivityIndicator } from 'react-native'
import FastImage from 'react-native-fast-image'
import { width } from '../configs';
import R from '../assets/R';

type Props = {
  uri?: any
}
export default class ImageView extends React.PureComponent<Props> {
  state = {
    uri: this.props.uri || '',
    loading: true,
    w: this.props.width,
    height: this.props.height,
  }

  static getDerivedStateFromProps(props) {
    return { uri: props.uri }
  }


  componentDidMount() {
    let { uri } = this.state
    let { updateSizes } = this.props

    Image.getSize(uri, (w, h) => {
      updateSizes && updateSizes(w, h, this.props.width)
      if (w > width && w > this.props.width && this.props.width && !this.props.height) {
        this.setState({
          w: this.props.width,
          height: h * (this.props.width / w),
          loading: false
        })
      } else if (!this.props.width && this.props.height) {
        this.setState({
          width: w * (this.props.height / h),
          height: this.props.height,
          loading: false
        })
      } else {
        this.setState({ w, height: h, loading: false })
      }
    })
  }

  render() {
    let { w, height, uri, loading } = this.state
    if (loading) {
      return <ActivityIndicator size="large" color={R.colors.lightBlueA401} />
    }
    return (
      <FastImage
        style={[{ width: w, height, resizeMode: 'cover', alignSelf: 'center' }, this.props.styleImage || {}]}
        source={{ uri }}
      />
    )
  }
}
