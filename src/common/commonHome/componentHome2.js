import React, { PureComponent } from 'react';
import { View, Text, ScrollView, FlatList, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { HEIGHT, getLineHeight, getFont, WIDTH } from '../../configs/Function';
import R from '../../assets/R';

class componentHome2 extends PureComponent {
  render() {
    const { styleView, styleTitle, title, data, renderItem } = this.props
    return (
      <View style={styleView}>
        <Text style={styleTitle}>{title}</Text>
        <ScrollView
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
        >
          <FlatList
            contentContainerStyle={{ alignSelf: 'flex-start' }}
            numColumns={Math.ceil(4)}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            data={data}
            renderItem={renderItem}
          />
        </ScrollView>
      </View>
    );
  }
}

export default componentHome2;
const styles = StyleSheet.create({
  title: {
    fontSize: getFont(16),
    lineHeight: getLineHeight(24),
    color: R.colors.black0,
    marginTop: HEIGHT(8)
  },
});
