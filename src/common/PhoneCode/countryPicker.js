import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Modal, Picker, FlatList, Image, TextInput } from 'react-native';
import PropTypes from 'prop-types';
import Flags from './resources/flags';
import Country from './country';
import styles from './styles';
import R from '../../assets/R';

const PickerItem = Picker.Item;

const propTypes = {
  buttonColor: PropTypes.string,
  labels: PropTypes.array,
  confirmText: PropTypes.string,
  cancelText: PropTypes.string,
  itemStyle: PropTypes.object,
  onSubmit: PropTypes.func,
  onPressCancel: PropTypes.func,
  onPressConfirm: PropTypes.func,
};

export default class CountryPicker extends Component {
  constructor(props) {
    super(props);
    let countries = Country.getAll()
    this.state = {
      buttonColor: this.props.buttonColor || R.colors.blueA701,
      modalVisible: false,
      selectedCountry: this.props.selectedCountry || countries[0],
      countryName: '',
      countries
    };

    this.onPressCancel = this.onPressCancel.bind(this);
    this.onPressSubmit = this.onPressSubmit.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    let countries = [...state.countries]
    let countryName = ''
    countries = countries.map((e, i) => {
      e.selected = false
      if (e.iso2 === props.selectedCountry) {
        e.selected = true
        countryName = e.name
      }
      return e
    })
    return { selectedCountry: props.selectedCountry, countries, countryName }
  }


  selectCountry(selectedCountry) {
    this.setState({
      selectedCountry,
    });
  }

  onPressCancel() {
    if (this.props.onPressCancel) {
      this.props.onPressCancel();
    }

    this.setState({
      modalVisible: false,
    });
  }

  onPressSubmit() {
    if (this.props.onPressConfirm) {
      this.props.onPressConfirm();
    }

    if (this.props.onSubmit) {
      this.props.onSubmit(this.state.selectedCountry);
    }

    this.setState({
      modalVisible: false,
    });
  }

  onValueChange(selectedCountry) {
    this.setState({
      selectedCountry,
    });
  }

  show() {
    this.setState({
      modalVisible: true,
    });
  }

  onSelectItem = (country) => () => {
    if (this.props.onSubmit) {
      this.props.onSubmit(country.iso2);
      this.setState({ modalVisible: false })
    }
    if (this.props.onChangeCountryData) {
      this.props.onChangeCountryData(country)
      this.setState({ modalVisible: false })
    }
  }

  renderItem = ({ item, index }) => (
    <TouchableOpacity
      onPress={this.onSelectItem(item)}
      style={styles.itemBtn}
    >
      <Image
        source={Flags.get(item.iso2)}
        style={[styles.flag, this.props.flagStyle]}
        onPress={this.onPressFlag}
      />
      <Text style={styles.itemName}>{item.name}</Text>
      {item.selected && <Image
        style={styles.iconChecked}
        source={R.images.checked}
      />}
    </TouchableOpacity>
  )

  render() {
    const { buttonColor, countries, countryName, selectedCountry } = this.state;
    const itemStyle = this.props.itemStyle || {};
    return (
      <Modal
        animationType="slide"
        transparent
        visible={this.state.modalVisible}
        onRequestClose={() => {
          console.log('Country picker has been closed.');
        }}
      >
        <View style={styles.basicContainer}>
          <View
            style={[
              styles.modalContainer,
              { backgroundColor: this.props.pickerBackgroundColor || R.colors.white },
            ]}
          >
            <View style={styles.buttonView}>
              <Image source={Flags.get(selectedCountry)} style={styles.flag} />
              <TouchableOpacity onPress={this.onPressCancel}>
                <Text style={[{ color: buttonColor }, this.props.buttonTextStyle]}>
                  {this.props.cancelText || 'Cancel'}
                </Text>
              </TouchableOpacity>
            </View>
            <TextInput
              placeholder="filter"
              style={styles.filterInput}
              onChangeText={this.filterCountry}
            />
            <View style={styles.mainBox}>
              <FlatList
                data={countries}
                extraData={countries}
                renderItem={this.renderItem}
                keyExtractor={(item) => item.ios2}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  filterCountry = val => {
    let countries = Country.getCountryByName(val)
    this.setState({ countries })
  }
}

CountryPicker.propTypes = propTypes;
