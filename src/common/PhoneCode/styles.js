import { StyleSheet, Dimensions } from 'react-native';
import R from '../../assets/R';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  filterInput: {
    borderWidth: 1,
    borderColor: R.colors.borderE,
    width: '100%',
    padding: 5,
    paddingHorizontal: 10
  },
  iconChecked: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 5
  },
  itemBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 8,
    justifyContent: 'space-between',
    width: '100%'
  },
  itemName: {
    width: '80%',
    textAlign: 'left',
    paddingLeft: 5
  },
  mainBox: {
    flex: 1,
    paddingBottom: 5,
    width: '100%'
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  basicContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: R.colors.black40p,
  },
  modalContainer: {
    alignItems: 'center',
    width: '90%',
    height: '85%',
    borderRadius: 3,
    borderWidth: 0
  },
  buttonView: {
    width: '100%',
    padding: 8,
    borderTopWidth: 0.5,
    borderTopColor: 'lightgrey',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  bottomPicker: {
    width: width * 0.8,
  },
  flag: {
    height: 20,
    width: 30,
    borderRadius: 2,
    borderWidth: 0.5,
    borderColor: R.colors.grey301,
    backgroundColor: R.colors.grey301,
  },
  text: {
    height: 20,
    padding: 0,
    justifyContent: 'center',
  },
});
