import React from 'react';
import { View, Text, StyleSheet, StatusBar, Image, Modal, TouchableOpacity } from 'react-native';
import R from '../assets/R';

export default class BaseAlert extends React.Component {
  state = {
    show: this.props.show || false,
  };

  static getDerivedStateFromProps(props, state) {
    return { show: props.show };
  }

  render() {
    let { show } = this.state;
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={show}
        onRequestClose={() => { console.log('onRequestClose') }}
      >
        <TouchableOpacity style={style.blackStyle} onPress={this.props.closeModal}>
          <StatusBar barStyle="light-content" backgroundColor={R.colors.black0} />
          <View style={style.body}>
            <View style={style.content}>
              <View style={style.headerView}>
                <Text style={style.headerText}>Thông báo</Text>
              </View>
              <Image source={R.images.circleCheck} style={style.circleCheck} />
              <Text style={style.message}>{this.props.message || 'Đã gửi thông báo khẩn'}</Text>
            </View>

          </View>
        </TouchableOpacity>
      </Modal>
    );
  }

  closeModal = () => {
    this.setState({ show: false })
  }
}

const style = StyleSheet.create({
  blackStyle: {
    flex: 1,
    backgroundColor: R.colors.black40p,
    justifyContent: 'center'
  },
  body: {
    width: '80%',
    backgroundColor: R.colors.white100,
    alignSelf: 'center',
    borderRadius: 10,
    padding: 10
  },
  iconBackStyle: {
    width: 25,
    height: 25
  },
  contentHeadStyle: {
    backgroundColor: R.colors.black40p,
    borderBottomWidth: 0
  },
  headerView: {
    borderBottomColor: R.colors.borderE,
    borderBottomWidth: 1,
    marginHorizontal: 20,
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  headerText: {
    color: R.colors.primaryColor,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center'
  },
  message: {
    fontSize: 15,
    fontWeight: '400',
    color: R.colors.black3,
    width: '80%',
    alignSelf: 'center',
    paddingVertical: 15,
    textAlign: 'center'
  },
  circleCheck: {
    alignSelf: 'center',
    marginVertical: 10,
    width: 40,
    height: 40,
    resizeMode: 'contain'
  }
});
