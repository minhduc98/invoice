import React from 'react';
import { StyleSheet, TouchableOpacity, View, Image } from 'react-native';
import env from 'src/env';
import R from '../assets/R';

export default class SmallAvatar extends React.PureComponent {
  state = {
    avatar: this.props.avatar,
    user: this.props.user || {}
  };

  static getDerivedStateFromProps(props, state) {
    return { avatar: props.avatar, user: props.user };
  }

  render() {
    let { onPress, btnAvatarStyle, borderColor, hidden } = this.props;
    let width = this.props.width || 94;
    let Avatar = this.returnAvatar()
    return (
      <View>
        <TouchableOpacity
          style={[
            style.content,
            btnAvatarStyle || {},
            hidden && style.hidden
          ]}
          onPress={onPress}
        >
          <Image
            style={Avatar.default ? { height: width, width }
              : {
                alignSelf: 'center',
                height: width,
                width,
                borderRadius: width / 2,
                borderWidth: 1,
                borderColor: borderColor || R.colors.primaryColor
              }
            }
            source={Avatar.source}
          // resizeMode="cover"
          />
        </TouchableOpacity>
      </View>

    );
  }

  returnAvatar = () => {
    let { avatar, user } = this.state;
    let { defaultAvatar } = this.props;
    if (avatar) {
      return { source: { uri: avatar.uri } };
    } else if (!!user.avatar) {
      // check avatar from social
      if (user.avatar.includes('https://') || user.avatar.includes('http://')) {
        return { source: { uri: user.avatar } };
      } else {
        return { source: { uri: `${env.serverURL}${user.avatar}` } };
      }
    } else {
      return { source: defaultAvatar || R.images.userBlue, default: true }
    }
  };
}

const style = StyleSheet.create({
  content: {
    justifyContent: 'center',
    alignSelf: 'center'
  },
  hidden: {
    opacity: 0
  }
});
