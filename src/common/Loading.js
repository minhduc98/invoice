import React from 'react'
import { View, ActivityIndicator, StyleSheet } from 'react-native'
import R from '../assets/R';

type Props = {
  loading: Boolean,
  style?: any
}

export default class Loading extends React.Component<Props> {
  constructor(props) {
    super(props)
    this.state = {
      loading: props.loading || false
    }
  }

  static getDerivedStateFromProps(props) {
    if (props.loading !== undefined) {
      return { loading: props.loading }
    }
    return null
  }

  render() {
    let { loading } = this.state
    let { style } = this.props
    let size = this.props.size || 'large'
    if (!loading) return null
    return (
      <View style={[styles.loading, style || {}]}>
        <ActivityIndicator size={size} color={R.colors.primaryColor} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  loading: {
    alignSelf: 'center'
  }
})
