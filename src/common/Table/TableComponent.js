import React, { Component } from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Table, Row } from 'react-native-table-component';
import colors from '../../assets/colors';
import { WIDTH, HEIGHT } from '../../configs/Function';

export default class TableComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  getTableData = data => {
    const tableData = [];
    for (let i = 0; i < data.length; i += 1) {
      const rowData = [];
      for (let j = 0; j < Object.keys(data[i]).length; j += 1) {
        rowData.push(...Object.values(data[i]));
      }
      tableData.push(rowData);
    }
    return tableData;
  };

  render() {
    const { data, tableHead, widthArr } = this.props;
    const tableData = this.getTableData(data);
    return (
      <View style={styles.container}>
        <ScrollView horizontal={true}>
          <View>
            <Table borderStyle={{ borderColor: colors.white65 }}>
              <Row
                data={tableHead}
                widthArr={widthArr}
                style={styles.header}
                textStyle={styles.text}
              />
            </Table>
            <ScrollView style={styles.dataWrapper}>
              <Table borderStyle={{ borderColor: colors.white65 }}>
                {tableData.map((rowData, index) => (
                  <Row
                    key={index}
                    data={rowData}
                    widthArr={widthArr}
                    style={[
                      styles.row,
                      index % 2 && { backgroundColor: colors.white95 },
                    ]}
                    textStyle={styles.text}
                  />
                ))}
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: WIDTH(16),
    paddingTop: HEIGHT(30),
    backgroundColor: '#fff'
  },
  header: {
    height: HEIGHT(50),
    backgroundColor: colors.colorMain
  },
  text: {
    textAlign: 'center',
    fontWeight: '100'
  },
  dataWrapper: {
    marginTop: HEIGHT(-1)
  },
  row: {
    height: HEIGHT(40),
    backgroundColor: colors.white90
  },
});
