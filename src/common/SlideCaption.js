import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import FastImage from 'react-native-fast-image'
import { width, height, ellipsis } from '../configs';
import R from '../assets/R';

type Props = {
  listImages?: any[],
  style: any
}

export default class SlideCaption extends React.Component<Props> {
  mapImage = () => {
    let listImages = this.props.listImages || []
    return (
      listImages.map((data) => (
        <View
          key={data.id}
          style={styles.imageView}
        >
          <FastImage
            source={{ uri: data.uri }}
            style={styles.image}
          />
          {!!data.caption && <Text style={styles.caption}>{ellipsis(data.caption, 80)}</Text>}
        </View>
      ))
    )
  }

  render() {
    let { style, slideWidth } = this.props
    return (
      <View
        style={[styles.slideView, slideWidth && { width: slideWidth }]}
      >
        {/* <Swiper
          {...this.props}
          // autoplay={true}
          showsButtons={false}
          style={style}
        >
          {this.mapImage()}
        </Swiper> */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  slideView: {
    width,
    height: height / 3
  },
  imageView: {
    flex: 1
  },
  image: {
    width: '100%',
    height: '100%'
  },
  caption: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    padding: 10,
    backgroundColor: R.colors.borderE30p,
    color: R.colors.black3,
    textAlign: 'center',
    zIndex: 10
  }
})
