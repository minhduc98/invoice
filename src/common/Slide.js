import React from 'react'
import { View, StyleSheet } from 'react-native'
import FastImage from 'react-native-fast-image'
import { width, height } from '../configs';

type Props = {
  listImages?: any[],
  style: any
}

export default class Slide extends React.Component<Props> {
  mapImage = () => {
    let listImages = this.props.listImages || []
    return (
      listImages.map((data) => (
        <FastImage
          key={data.id}
          source={{ uri: data.image }}
          style={styles.slideView}
        />
      ))
    )
  }

  render() {
    let { style } = this.props
    return (
      <View style={styles.slideView}>
        {/* <Swiper
          {...this.props}
          // autoplay={true}
          showsButtons={false}
          style={style}
        >
          {this.mapImage()}
        </Swiper> */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  slideView: { width, height: height / 3 }
})
