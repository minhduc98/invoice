import React from 'react';
import { Image, StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { getLangCode, ellipsis } from '../configs';
import NavigationService from '../routers/NavigationService';
import R from '../assets/R';

export default class BaseHeader extends React.PureComponent {
  state = {
    langCode: getLangCode(),
  }

  render() {
    let { langCode } = this.state
    let { title, onPress, onLeftPress, customizeLeftAction, textLeft, textRight, titleStyle, hideBack, rightIcon, rightIconStyle, logo, logoStyle, contentStyle, leftIconStyle, leftIcon } = this.props
    return (
      <View style={[style.content, hideBack && !customizeLeftAction && style.flexEnd, logo && style.paddingVertical, contentStyle || {}]}>
        {!hideBack
          && <TouchableOpacity
            onPress={onLeftPress || NavigationService.pop}
            style={style.p10}
          >
            <Image source={leftIcon || R.images.backDark} style={[style.back, leftIconStyle || {}]} />
             </TouchableOpacity>
        }

        {onLeftPress && customizeLeftAction
          && <TouchableOpacity
            onPress={onLeftPress}
          >
            <Text style={style.leftLableStyle}>{textLeft}</Text>
             </TouchableOpacity>
        }

        <View style={[style.midStyle, hideBack && !onPress ? style.flex : style.absolute]}>
          {!logo && <Text style={[style.title, titleStyle || {}]}>{ellipsis(title || '')}</Text>}
          {logo && <Image style={[style.logo, logoStyle || {}]} source={logo} />}
        </View>

        {onPress && !rightIcon
          && <TouchableOpacity
            onPress={onPress}
          >
            <Text style={style.share}>{textRight}</Text>
             </TouchableOpacity>
        }
        {onPress && rightIcon
          && <TouchableOpacity
            style={style.btnRightIcon}
            onPress={onPress}
          >
            <Image style={[style.iconStyle, rightIconStyle || {}]} source={rightIcon} />
             </TouchableOpacity>
        }

      </View>
    )
  }
}

const style = StyleSheet.create({
  flex: {
    flex: 1
  },
  title: {
    fontWeight: '600',
    fontSize: 15
  },
  flexEnd: {
    justifyContent: 'flex-end',
  },
  iconStyle: {
    height: 16,
    width: 16,
    resizeMode: 'contain'
  },
  paddingVertical: {
    paddingVertical: 5
  },
  logo: {
    alignSelf: 'center',
    width: 70,
    resizeMode: 'contain'
  },
  midStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    paddingVertical: 5,
    zIndex: 10,
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: '20%',
    right: '20%',
    bottom: 0,
  },
  p10: {
    padding: 10
  },
  btnRightIcon: {
    paddingHorizontal: 10,
    paddingVertical: 5
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: R.colors.bgHeader,
    borderBottomColor: R.colors.borderE,
    borderBottomWidth: 0.5
  },
  back: {
    resizeMode: 'contain',
    width: 15,
    height: 15
  },
  share: {
    fontSize: 14,
    fontWeight: '600',
    color: R.colors.primaryColor,
    padding: 10
  },
  leftLableStyle: {
    fontSize: 14,
    color: R.colors.red300,
    padding: 10
  }
})
