/* eslint-disable react/no-unused-state */
import React from 'react'
import { TouchableWithoutFeedback } from 'react-native'
import env from 'src/env'
import FastImage from 'react-native-fast-image'
import { width, height } from '../configs';


export default class FastImageView extends React.PureComponent {
  state = {
    image: this.props.image || {},
    screenWidth: this.props.screenWidth || width,
    noResizeHeight: this.props.noResizeHeight || false,
  }

  static getDerivedStateFromProps(props) {
    return { image: props.image }
  }

  render() {
    let { screenWidth, image, noResizeHeight } = this.state
    if (!image.src) return null

    let ratio = image.width / image.height
    let imgWidth = image.width >= screenWidth ? screenWidth : image.width
    let resizeHeight = imgWidth / ratio
    let imgHeight = resizeHeight >= height * 0.75 ? height * 0.75 : resizeHeight
    return (
      <TouchableWithoutFeedback onPress={this.props.onPress}>
        <FastImage
          pointerEvents="auto"
          source={{ uri: image.uri ? image.uri : `${env.serverURL}${image.src}` }}
          style={[{ width: imgWidth }, noResizeHeight ? { aspectRatio: ratio } : { height: imgHeight }]}
          resizeMode={!noResizeHeight ? 'cover' : null}
        />
      </TouchableWithoutFeedback>

    )
  }
}
