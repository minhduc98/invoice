import React from 'react'
import { View, Text, TextInput, StyleSheet, TextInputProps, TouchableOpacity, Image } from 'react-native'
import R from '../assets/R';

type InputProps = TextInputProps & {
  label: string,
  textEnter?: string,
  removeSpace?: boolean,
  isPrice?: boolean,
  isDate?: boolean,
  inputViewStyle?: any,
  labelStyle?: any,
  inputStyle?: any,
  icon?: any,
}

export default class InputLabel extends React.PureComponent<InputProps> {
  constructor(props) {
    super(props)
    this.state = {
      value: props.value || ''
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.value !== '') return { value: props.value }
    return null
  }

  render() {
    let { value } = this.state
    let { textEnter, label, multiline, inputViewStyle, labelStyle, inputStyle, icon, iconStyle, onPressIcon, placeholder } = this.props
    let hint = textEnter || ''
    if (placeholder) {
      hint += placeholder
    } else {
      hint += label
    }
    return (
      <View style={[style.inputViewStyle, inputViewStyle || {}]}>
        <View style={style.flexRowBetween}>
          <Text style={[style.labelStyle, labelStyle || {}]}>{label}</Text>
          {icon
            && <TouchableOpacity
              style={style.btnIcon}
              onPress={onPressIcon}
            >
              <Image
                style={[style.iconStyle, iconStyle || {}]}
                source={icon}
              />
               </TouchableOpacity>
          }
        </View>
        <TextInput
          {...this.props}
          value={value}
          onChangeText={this.onChangeText}
          style={[style.inputStyle, inputStyle || {}, multiline && style.multiline]}
          placeholder={hint}
          placeholderTextColor={R.colors.borderC7}
        />
      </View>
    )
  }

  onChangeText = text => {
    let { removeSpace, isPrice, isDate } = this.props
    if (removeSpace) text = text.replace(/ /g, '') // xóa khoảng cách
    if (isPrice) {
      // eslint-disable-next-line no-useless-escape
      text = text.replace(/\,/g, '')
      text = this.toPrice(text) // trả vể định dạng 123,456,789
    }
    if (isDate) {
      // eslint-disable-next-line no-useless-escape
      text = text.replace(/\//g, '')
      text = this.toDate(text) // trả vể định dạng DD/MM
    }
    this.setState({ value: text })
  }

  toPrice = str => (str ? str.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : '0')

  toDate = str => (str ? str.toString().replace(/(\d)(?=(\d{2})+(?!\d))/g, '$1/') : '')

  getValue = () => this.state.value
}

const style = StyleSheet.create({
  labelStyle: {
    paddingLeft: 10,
    fontSize: 16,
    paddingBottom: 8,
    color: R.colors.black5
  },
  inputStyle: {
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: R.colors.borderD,
    borderWidth: 0.5,
    borderRadius: 3
  },
  inputViewStyle: {
    marginTop: 10
  },
  multiline: {
    paddingTop: 5,
    paddingBottom: 5
  },
  flexRowBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  iconStyle: {
    width: 18,
    height: 18,
    resizeMode: 'contain'
  },
  btnIcon: {
    paddingLeft: 10,
    paddingRight: 10
  }
})
