import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, Modal, TouchableOpacity, StatusBar } from 'react-native';
import R from '../assets/R';

export default class BaseOptions extends React.Component {
  state = {
    show: this.props.show || false,
    data: []
  };

  static getDerivedStateFromProps(props, state) {
    let { value, show } = props
    let data = [...props.data]
    data = data.map(e => {
      e.active = false
      if (e.value === value) {
        e.active = true
      }
      return e
    })
    return { data, show }
  }

  render() {
    let { show, data } = this.state;
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={show}
        onRequestClose={() => { console.log('onRequestClose') }}
      >
        <TouchableOpacity style={style.blackStyle} onPress={this.props.closeOptions}>
          <StatusBar barStyle="light-content" backgroundColor={R.colors.black40p} />
          <View style={style.body}>
            <View style={style.content}>
              <FlatList
                data={data}
                extraData={data}
                renderItem={this.renderItem}
                keyExtractor={this.keyExtractor}
              />
            </View>
          </View>
        </TouchableOpacity>

      </Modal>
    );
  }

  renderItem = ({ index, item }) => (
    <TouchableOpacity
      onPress={this.onSelect(item)}
      style={style.btn}
    >
      <Text style={style.label}>{item.name}</Text>
      {item.active
          && <Image
            style={style.icon}
            source={R.images.circleCheck}
          />
        }
    </TouchableOpacity>
  )

  onSelect = value => () => {
    let { setValue, closeOptions } = this.props
    setValue && setValue(value)
    closeOptions()
  }

  keyExtractor = (item, index) => item.value.toString()
}

const style = StyleSheet.create({
  label: {
    fontSize: 15,
    color: R.colors.black1
  },
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
    paddingVertical: 10
  },
  icon: {
    marginRight: 8,
    width: 18,
    height: 18,
    resizeMode: 'contain'
  },
  iconMark: {
    width: 18,
    height: 18,
    resizeMode: 'contain'
  },
  blackStyle: {
    flex: 1,
    backgroundColor: R.colors.black40p,
    justifyContent: 'center'
  },
  body: {
    width: '80%',
    backgroundColor: R.colors.white100,
    alignSelf: 'center',
    borderRadius: 10,
    padding: 10
  },
});
