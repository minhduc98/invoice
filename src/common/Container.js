import React, { PureComponent } from 'react';
import { View, StyleSheet, Platform, StatusBar } from 'react-native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import R from '../assets/R';

// const DismissKeyboard = ({ children }) => (
//   <TouchableWithoutFeedback
//     onPress={() => Keyboard.dismiss()}
//   >
//     {children}
//   </TouchableWithoutFeedback>
// )

class Container extends PureComponent {
  static defaultProps = {
    statusBarColor: R.colors.primaryColor,
    barStyle: 'dark-content',
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let { statusBarColor } = this.props
    let backgroundColor = statusBarColor || R.colors.primaryColor
    return (
      <View style={[styles.container, { backgroundColor }, this.props.style]}>
        <StatusBar
          backgroundColor={statusBarColor || R.colors.primaryColor}
          barStyle={this.props.barStyle || 'dark-content'}
        />
        <View style={[styles.subContainer, this.props.containerStyle]}>{this.props.children}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.primaryColor,
    paddingTop: Platform.OS === 'ios' ? getStatusBarHeight() : 0,
  },

  subContainer: {
    flex: 1,
    backgroundColor: R.colors.white100,
  },
});

export default Container;
