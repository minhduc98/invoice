import React from 'react';
import { TouchableOpacity, StyleSheet, View, Image, Text } from 'react-native';
import R from '../assets/R';

type CheckBoxProps = {
  checked: Boolean, // checkbox value
  label: String, // tên checkbox
  labelStyle?: any, // style cho label
  subLabel?: String, // mô tả cho checkbox
  subLabelStyle?: any, // style cho subLabel
  contentStyle?: any, // style cho component
  iconUnCheck?: any, // image khi checked = false
  iconChecked?: any, // image khi checked = true
  iconStyle?: any, // style cho icon
  setValue?: () => void, // thay đổi trạng thái check
};

export default class CheckBox extends React.Component<CheckBoxProps> {
  constructor(props) {
    super(props);
    this.state = {
      checked: props.checked,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.checked !== undefined) {
      return { checked: nextProps.checked };
    }
    return null;
  }

  render() {
    const { checked } = this.state;
    const {
      label,
      subLabel,
      labelStyle,
      subLabelStyle,
      contentStyle,
      iconChecked,
      iconStyle,
      iconUnCheck,
    } = this.props;

    return (
      <View style={[style.flexRow, style.mb, contentStyle || {}]}>
        <TouchableOpacity onPress={this.onStateChange}>
          <Image
            style={[style.icon, iconStyle || {}]}
            source={checked ? iconChecked || R.images.checkBox : iconUnCheck || R.images.unCheckBox}
          />
        </TouchableOpacity>
        <View style={style.flex}>
          <Text style={[style.label, labelStyle || {}]}>
            {label}
            {subLabel && <Text onPress={this.props.onPress} style={[style.subLabel, subLabelStyle || {}]}>{` ${subLabel}`}</Text>}
          </Text>
        </View>
      </View>
    );
  }

  onStateChange = () => {
    const { checked } = this.state;
    const { setValue } = this.props;
    if (setValue) setValue(!checked);
    this.setState({ checked: !checked });
  };

  // eslint-disable-next-line react/destructuring-assignment
  getValue = () => this.state.checked;
}

const style = StyleSheet.create({
  flex: {
    flex: 1
  },
  flexRowBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  flexRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  space: {
    padding: 10
  },
  mb: {
    marginBottom: 10
  },
  icon: {
    height: 19,
    width: 19,
    resizeMode: 'contain',
    marginLeft: 10,
    marginRight: 10
  },
  label: {
    color: R.colors.black3,
    fontSize: 13
  },
  subLabel: {
    color: R.colors.black9,
    fontSize: 13
  }
});
