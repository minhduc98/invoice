import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import R from '../assets/R';

type BtnProps = {
  btnStyle?: any, // style cho button
  textStyle?: any, // style cho text
  onPress?: Function, // function thực hiện
  name: string, // Tên button,
  backgroundColor?: string, // màu nền button
  textColor?: string, // màu nền button
};

export default class Btn extends React.PureComponent<BtnProps> {
  render() {
    let { onPress, btnStyle, textStyle, name, backgroundColor, textColor } = this.props;
    return (
      <TouchableOpacity
        onPress={onPress || null}
        style={[styles.btn, { backgroundColor: backgroundColor || R.colors.primaryColor }, btnStyle || {}]}
      >
        <Text style={[styles.txt, { color: textColor || R.colors.white100 }, textStyle || {}]}>
          {name}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  txt: {
    alignSelf: 'center',
    fontSize: 14,
    color: R.colors.white100,
    fontWeight: '500',
  },
  btn: {
    backgroundColor: R.colors.primaryColor,
    width: '85%',
    alignSelf: 'center',
    marginTop: 25,
    marginBottom: 25,
    borderRadius: 3,
    height: 38,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
});
