import React from 'react'
import { TextInput, View, Image, StyleSheet, TextInputProps } from 'react-native'
import R from '../assets/R';

type InputProps = TextInputProps & {
  styleIcon?: any,
  icon?: any,
  other?: Boolean, // other style
  customStyle?: any,
  contentStyle?: any,
  showIcon?: Boolean,
  removeSpace?: Boolean,
}
export default class InputIcon extends React.PureComponent<InputProps> {
  constructor(props) {
    super(props)
    this.state = {
      value: props.value || '',
      showIcon: props.showIcon || true
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.value !== '') return { value: props.value }
    return null
  }

  render() {
    let { showIcon, value } = this.state
    let { styleIcon, placeholderTextColor, icon, customStyle, contentStyle, other } = this.props
    return (
      <View style={[style.inputView, other && style.inputView2, contentStyle || {}]}>
        {showIcon && <Image
          style={[style.icon, other && style.w14, styleIcon || {}]}
          source={icon}
        />}
        <TextInput
          {...this.props}
          placeholderTextColor={other ? R.colors.black5 : placeholderTextColor || R.colors.lightBlue901}
          value={value}
          onChangeText={this.onChangeText}
          style={customStyle || [style.input, other && style.input2, customStyle || {}]}
        />
      </View>
    )
  }

  onChangeText = text => {
    let { removeSpace } = this.props
    if (removeSpace) text = text.replace(/ /g, '')
    this.setState({ value: text })
  }

  getValue = () => this.state.value
}

const style = StyleSheet.create({
  input: {
    color: R.colors.lightBlueA401,
    padding: 0,
    paddingLeft: 8,
    fontSize: 14,
    backgroundColor: R.colors.white,
    flex: 1
  },
  inputView: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: R.colors.cyan51,
    width: '80%',
    alignSelf: 'center',
    alignItems: 'center',
    marginBottom: 15,
    paddingBottom: 0
  },
  input2: {
    borderWidth: 1,
    borderColor: R.colors.borderE,
    backgroundColor: R.colors.grey95,
    flex: 1,
    padding: 5,
    borderRadius: 3,
    color: R.colors.black3,
    marginLeft: 10
  },
  inputView2: {
    borderBottomWidth: 0,
    width: '100%'
  },
  icon: {
    width: 12,
    resizeMode: 'contain'
  },
  w14: {
    width: 14
  }
})
