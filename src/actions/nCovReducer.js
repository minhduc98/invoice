
import { UPDATE_NEWS } from './actionTypes';

export function updateNews(news) {
  return {
    type: UPDATE_NEWS,
    payload: news
  }
}
