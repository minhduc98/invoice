import { UPDATE_USER_INFO, UPDATE_USER_DATA, EDIT_USER_PROFILE } from 'actions';

const initialState = {
  data: null,
  token: null,
  langCode: null
};

export const userReducers = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_USER_INFO:
      return {
        ...state,
        data: action.payload.data || action.payload.user,
        token: action.payload.token
      };
    case UPDATE_USER_DATA:
      return {
        ...state,
        data: action.payload.data,
        token: action.payload.token
      };
    case EDIT_USER_PROFILE:
      return {
        ...state,
        data: { ...state.data, ...action.payload.data }
      };
    default:
      return state;
  }
};
