import { UPDATE_NEWS } from '../actions';

const initialState = {
  news: { infected_numbers: 34986,
    deaths_numbers: 725,
    affected_countries_numbers: 28 }
};

export const locationReducers = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_NEWS:
      return {
        ...state,
        location: action.payload
      };

    default:
      return state;
  }
};
